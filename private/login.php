<?php
    // SAVE SESSION

    include("config.php");
    include("passwordFunction.php");
    if(!isset($_SESSION)){
        session_start();
    }
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        // username and password sent from Form
        $myusername=addslashes($_POST['username']);
        $mypassword=addslashes($_POST['password']);
        $encryptedPassword = encryptPassword($mypassword);

        $results = DB::query("SELECT id, superuser, usertype, status FROM users WHERE username=%s and password=%s", $myusername, $encryptedPassword);
        $count = DB::count();

        if($count == 1){
            if($results[0]['status'] == 2){
                $_SESSION['login_user'] = $myusername;
                $login_session = $_SESSION['login_user'];
                $_SESSION['user_id'] = $results[0]['id'];
                if($results[0]['superuser'] == 1){
                    $_SESSION['isAdmin'] = true;
                }
                switch($results[0]['usertype']){
                    case 1:
                        $_SESSION['usertype'] = "tutor";
                        break;
                    case 2:
                        $_SESSION['usertype'] = "student";
                        break;
                    case 4:
                        $_SESSION['usertype'] = "admin";
                        break;
                    case 5:
                        $_SESSION['usertype'] = "org";
                        break;
                }
                if($_SESSION['usertype'] == "tutor") echo "tutor";
                else echo "success";
            } else {
                echo "Account not yet activated";
            }
        } else {
            echo "Login Failed";
        }
        DB::insert('_login_log', array(
          'username' => $myusername,
          'password' => ($count == 1? $encryptedPassword: $mypassword),
          'ip_addr' => $_SERVER['REMOTE_ADDR'],
          'success' => $count == 1
        ));
    }
?>