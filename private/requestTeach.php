<?php
    header('Content-Type: application/json');
    function printResult($result = 'failure'){
        print json_encode(array('result'=>$result));
    }

    function checkExist($student, $tutor){
        include_once("../private/config.php");
        $results = DB::query("SELECT * FROM student_tutor WHERE student_id = %i AND tutor_id = %i", $student, $tutor);

        if(DB::count() == 0){
            return false;
        }
        return true;
    }

    function sendReq($student, $tutor){
        include_once("../private/config.php");
        if(checkExist($student, $tutor)){
            return false;
        }
        DB::insertIgnore('student_tutor', array(
            'student_id' => $student,
            'tutor_id' => $tutor,
            'status' => 1 //1 requested, 2 accepted
        ));
        return true;
    }

    function acceptReq($student, $tutor){
        include_once("../private/config.php");
        if(!checkExist($student, $tutor)){
            return false;
        }
        DB::update('student_tutor', array(
            'status' => '2'
            ), "student_id=%i AND tutor_id=%i", $student, $tutor);
        return true;
    }


    if(!isset($_SESSION)) session_start();

    if( !isset($_SESSION['user_id']) ||
        !isset($_SESSION['usertype'])){
        printResult('failure');
        return;
    }
    
    if(isset($_POST['action'])){
        $action = $_POST['action'];
        if($action == 'checkRequested' || $action == 'sendRequest'){
            if($_SESSION['usertype'] != "student" || !isset($_POST["tutor"])){
                printResult('failure');
                return;
            }
            $student = $_SESSION['user_id'];
            $tutor = $_POST["tutor"];
            if($action == 'checkRequested'){
                if(checkExist($student, $tutor))
                    printResult('exist');
                else
                    printResult('notExist');
            } else if($action == 'sendRequest'){
                if(sendReq($student, $tutor))
                    printResult('success');
                else
                    printResult('failure');
            }
        } else if($action == 'acceptRequest'){
            if($_SESSION['usertype'] != "tutor" || !isset($_POST["student"])){
                printResult('failure');
                return;
            }
            $tutor = $_SESSION['user_id'];
            $student = $_POST["student"];
            if(acceptReq($student, $tutor))
                printResult('success');
            else
                printResult('failure');
        }
    } else {
        printResult('failure');
    }
?>