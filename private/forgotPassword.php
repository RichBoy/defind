<?php
    include("config.php");
    include("passwordFunction.php");
    header('Content-Type: application/json');

    if(isset($_POST['email'];)){
        $email = $_POST['email'];
        $pw = randomPassword();
        forgotPw($email, $pw);
        sendEmail($email, $pw);
    }


function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function forgotPw($email, $pw){
    $result = DB::query("SELECT `id` FROM users WHERE email = %s", $email);
    if(DB::count() == 1){
        $encPW = encryptPassword($pw);
        $updateUserArr = array(
            'password' => $encPW
        );
        DB::update('users', $updateUserArr , "email=%s", $email);
    };
}

function sendEmail($email, $pw){
    $message = "Your new password: ".$pw."\n\n";
    $message .= 'www.defind.com.hk/pages/login.php';

    if(mail($email, 'Defind Password Confirmation', $message, 'From:info@defind.com.hk')){
        $arr['result'] = 'success';
    } else {
        $arr['result'] = 'failed';
        $arr['errorMsg'] = 'Cannot send email.';
    }

    echo json_encode($arr);
}
?>