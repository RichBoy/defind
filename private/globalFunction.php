<?php
    // include '../../chromephp/ChromePhp.php';
    // ChromePhp::log('Hello console!');
    // ChromePhp::log($_SERVER);
    // ChromePhp::warn('something went wrong!');
    $FILTER_WORDS = array();
    $FILTER_WORDS_REPLACE = array();
    function loadFilter(){
        global $FILTER_WORDS, $FILTER_WORDS_REPLACE;
        $content = file_get_contents('../data/global.json');
        $contentObj = json_decode($content);
        foreach($contentObj as $index => $value){
            $FILTER_WORDS[] = $index;
            $FILTER_WORDS_REPLACE[] = $value;
        }
    }
    loadFilter();

    $LANGUAGE = language();

    function filter($object){ // str_replace not supporting nested array, this function does!
        global $FILTER_WORDS, $FILTER_WORDS_REPLACE;
        if(is_array($object)){
            foreach($object as $index => $value){
                if(is_array($value)){
                    $object[$index] = filter($value);
                } else {
                    $object[$index] = str_replace($FILTER_WORDS, $FILTER_WORDS_REPLACE, $value);
                }
            }
        } else {
            $object[$index] = str_replace($FILTER_WORDS, $FILTER_WORDS_REPLACE, $value);
        }
        return $object;
    }

    // Return an php object from .json file
    function getJSON($url){
        $content = file_get_contents($url);
        $contentArr = json_decode($content, true);
        return filter($contentArr);
    }

    // Return an php object from .json file
    function getLangJSON($url){
        global $LANGUAGE;
        $content = file_get_contents($url);
        $contentArr = json_decode($content, true);
        //$language = language();
        return filter($contentArr[$LANGUAGE]);
    }

    //profile related:

    function getTutorDetail($id){
        include_once("../private/config.php");
        $results = DB::query("SELECT description, price_range, availability FROM tutor_details WHERE user_id = %i", $id);
        if(DB::count() == 1){
            return $results[0];
        }
        return array(
                "description" => "TEST",
                "price_range" => 1,
                "availability" => 0
            );
    }

    function getProfileDescription($id){
        $tutorDetail = getTutorDetail($id);
        return $tutorDetail['description'];
    }

    function getProfileAvailability($id){
        // $tutorDetail = getTutorDetail($id);
        // return $tutorDetail['availability'];
        return 1;
    }

    function getProfile($id){
        include_once("../private/config.php");
        $results = DB::query("SELECT `lastname`, `firstname`, `availability`, `birth_year`, `university_id`, `faculty_id`, `short_name` university, `gender`, `region_id`, `chinese_name`, GROUP_CONCAT(pd.district_id) district_ids, `school`, `gpa` FROM profiles p LEFT OUTER JOIN `universities` u ON university_id = u.id LEFT JOIN `profile_districts` pd ON p.user_id = pd.user_id WHERE p.user_id = %i", $id);
        if(DB::count() == 0) return json_decode('{"lastname":"N/A","firstname":"","availability":"0","birth_year":"N/A","university_id":"8","university":"N/A","gender":"","region_id":"","chinese_name":"","school":"", "gpa":""}', true);
        return $results[0];
    }

    function getSubjectList($id = -1){ // get subject list with availability flag by user_id
        include_once("../private/config.php");
        if($id == -1) $results = DB::query("SELECT DISTINCT types.id typeId, types.name typeName , subj.id subjectId, subj.name subject, (SELECT COUNT(user_id) from tutor_listing_subjects where user_id = %i and subject_id = subj.id) available FROM study_subjects subj LEFT JOIN study_types types ON subj.study_id = types.id  ORDER BY typeId, subj.id", $_SESSION['user_id']);
        else $results = DB::query("SELECT DISTINCT types.id typeId, types.name typeName , subj.id subjectId, subj.name subject, (SELECT COUNT(user_id) from tutor_listing_subjects where user_id = %i and subject_id = subj.id) available FROM study_subjects subj LEFT JOIN study_types types ON subj.study_id = types.id WHERE subj.id in (SELECT subject_id FROM tutor_listing_subjects WHERE user_id = %i) ORDER BY typeId, subj.id", $id, $id);
        return $results;
    }

    function simplify($arr){ // convert into study type based object
        $result = array(); // included everything

        $typeObj = array(); // object that contains type data, reset after each loop
        $subjects = array(); // arrays of subject for each type
        foreach($arr as $key => $value){
            $subject = array('name'=>$value['subject'], 'available'=>$value['available'], 'id'=>$value['subjectId']);
            $typeObj['typeId'] = $arr[$key]['typeId'];
            $subjects[] = $subject;
            if($key + 1 < count($arr)){ // next one not the last one 
                if($arr[$key]['typeId'] != $arr[$key + 1]['typeId']){ // next one not the same type
                    $typeObj['name'] = $value['typeName'];
                    $typeObj['subjects'] = $subjects;
                    $result[] = $typeObj;
                    $typeObj = array();
                    $subjects = array();
                    // reset the temp type and subject array after adding into result array
                }
            } else { // last one
                $typeObj['name'] = $value['typeName'];
                $typeObj['subjects'] = $subjects;
                $result[] = $typeObj;
            }
        }
        return $result;
    }

    // search related:

    function searchTutor($additionalParam = array()){
        include_once('../private/config.php');
        if(!isset($_SESSION)){
            session_start();
        }
        $searchParam = $_SESSION['searchParam'];

        foreach($searchParam['subjects_id'] as $value){
            // prevent sql injection
            if (!is_numeric($value)) {
                return false;
            }
        }

        // TODO
        // GROUP_CONCAT(districts)
        $where = new WhereClause('and');
        $where->add('subject_id in (%s)', implode(",", $searchParam['subjects_id']));
        $where->add('tl.study_id=%i', $searchParam['study_id']);
        $where->add('lastname<>%s','');
        $where->add('teaching_status=%i', 1);

        $results = DB::query("SELECT `users`.`id` id, CONCAT(`firstname`,' ', `lastname`) name, `gender`, `price`, `details` description, GROUP_CONCAT(ss.name SEPARATOR ',') subjects\n"
            . "FROM `tutor_listing_subjects` tls \n"
            . "LEFT OUTER JOIN `users` ON tls.user_id = users.id \n"
            . "LEFT OUTER JOIN `profiles` ON tls.user_id = profiles.user_id \n"
            . "LEFT OUTER JOIN `tutor_listings` tl ON tls.user_id = tl.user_id \n"
            . "LEFT OUTER JOIN `study_subjects` ss ON subject_id = ss.id \n"
            // . "LEFT OUTER JOIN (SELECT pd.user_id user_id, GROUP_CONCAT(district_id) FROM profile_districts pd WHERE pd.user_id = tls.user_id GROUP BY pd.user_id) dist ON tls.user_id = dist.user_id \n"
            . "WHERE %l GROUP BY tls.user_id", $where->text());
        return addslashes(json_encode($results));
    }

    function getDistrictsByRegion($rid){
        // include_once("../private/config.php");
        if(!isset($_SESSION)){
            session_start();
        }
        $districts = getLangJSON("../data/districts.json");
        
        // $where = new WhereClause('and');
        if(!isset($rid)){
            // return array_merge(array_merge($districts[1],$districts[2]),$districts[3]);
            $districtArr = array();
            foreach ($districts as $inner){
                $districtArr = array_merge($districtArr, $inner);
            }
            return $districtArr;
        }
        if($rid >= 1 and $rid <= 3){
            return $districts[$rid];
        } else {
            $items = array();
            foreach ($districts as $inner){
                $items = array_merge($items, $inner);
            }
            return $items; 
        }
        // $results = DB::query("SELECT id, name FROM `location_districts` WHERE %l", $where->text());
        // return $results;
    }

    function getAllTutor(){
        include_once('../private/config.php');
        
        $results = DB::query("SELECT u.`id` id, CONCAT(`firstname`,' ', `lastname`) name, `email`, `status`\n"
            . "FROM `users` u \n"
            . "LEFT OUTER JOIN `profiles` ON u.id = profiles.user_id \n"
            . "GROUP BY u.id ORDER BY name");
        return $results;
    }

    function getProfileEdit($id){
        include_once("../private/config.php");
        $results = DB::query("SELECT `lastname`, `firstname`, `chinese_name`, `availability`, `birth_year`, `university_id`, `gender`, `region_id`, `username`, `email`, `status`, `verified` FROM users LEFT OUTER JOIN `profiles` p ON id = p.user_id WHERE id = %i", $id);
        if(DB::count() == 0) return "{}";
        return json_encode($results[0]);
    }

    function checkHasEmail($email){
        DB::query("SELECT id FROM users WHERE email=%s", $email);
        return DB::count();
    }

    function checkHasUsername($u){
        DB::query("SELECT id FROM users WHERE username=%s", $u);
        return DB::count();
    }

    function language(){
        # function returns longhand form of the language. 
        # function expects longhand form of the language in the  
        # $_GET variable to switch languages (?language=english)  
         
        // Set variables 
        $default_language = 'english'; 
        $supported_languages = array( 
            'en' => 'english', 
            'ch' => 'chinese' 
        ); 
         
        // Client is setting language with query string '?language=english             
        if((!empty($_GET['language'])) and (in_array($_GET['language'], $supported_languages))) 
        { 
            setcookie ('language', $_GET['language'], time()+100000000, '/', '', 0); 
            return $_GET['language']; 
        } 
         
        // Check for preset language in $_COOKIE['language'] 
        if((!empty($_COOKIE['language'])) and (in_array($_COOKIE['language'], $supported_languages))) 
        { 
            return $_COOKIE['language']; 
        } 
         
        // If none of the above the script finds the first 
        // matching browser language or returns the language 
        // in the $default_language variable on no match 
        if(empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) 
        { 
            return $default_language; 
        } 
        $bar = ''; # to use in regex pattern 
        $pattern = '';
        foreach($supported_languages as $k => $v){ 
            $pattern .= $bar.$k; 
            $bar = '|'; 
        } 
        if(preg_match('/'.$pattern.'/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches)) 
        { 
            return $supported_languages[$matches['0']]; 
        } 
        return $default_language; 
    } 

    function getDistrictsByID($id){
        $districts = getLangJSON('../data/districts.json');
        $districtArr = array();
        foreach ($districts as $inner){
            $districtArr = array_merge($districtArr, $inner);
        }
        $districtStr = '';
        $ids = explode( ',', $id);
        for($j = 0; $j< sizeof($ids); $j++){
            for($k = 0; $k< sizeof($districtArr); $k++){
                if($districtArr[$k]['id'] == $ids[$j]){
                    $districtStr .= $districtArr[$k]['name'].',';
                    break;
                }
            }
        }
        return substr($districtStr, 0, -1);
    }

    function getStudyTypeID($id){
        $studyType = getLangJSON('../data/studyType.json');
        return $studyType[$id];
    }

    function getUniversityID($id){
        $university = getLangJSON('../data/universityList.json');
        return $university[$id];
    }

    function getRegionID($id){
        $region = getLangJSON('../data/regionList.json');
        return $region[$id];
    }

    function getSubjectByID($id){
        $subjects = getLangJSON('../data/subject.json');
        $subjectArr = array();
        foreach ($subjects as $inner){
            $subjectArr = array_merge($subjectArr, $inner);
        }
        $subjectStr = '';
        $ids = explode( ',', $id);
        for($j = 0; $j< sizeof($ids); $j++){
            for($k = 0; $k< sizeof($subjectArr); $k++){
                if($subjectArr[$k]['id'] == $ids[$j]){
                    $subjectStr .= $subjectArr[$k]['name'].',';
                    break;
                }
            }
        }
        return substr($subjectStr, 0, -1);
    }

    function getUniversityByName($name){
        $langArr = getJSON('../data/universityList.json');
        global $LANGUAGE;
        $key = 0;
        if(array_search($name, $langArr['english'])!==false){
            $key = array_search($name, $langArr['english']);
        } else if(array_search($name, $langArr['chinese'])!==false){
            $key = array_search($name, $langArr['chinese']);
        }
        return $langArr[$LANGUAGE][$key];
    }

    function getAvgRate($id = 0){
        include_once("../private/config.php");
        $results = DB::query("SELECT COUNT(*) total, ROUND(AVG(`attitude`)) avgAttitude, ROUND(AVG(`clarity`)) avgClarity, ROUND(AVG(`knowledge`)) avgKnowledge, ROUND(AVG(`preparedness`)) avgPreparedness FROM `tutor_rating` WHERE tutor_id = %d", $id);
        if(DB::count() == 0 || $results[0]['total'] == 0) return array();
        return $results[0];
    }

    function getRate($id = 0){
        include_once("../private/config.php");
        $results = DB::query("SELECT username studentName, attitude, clarity, knowledge, preparedness, comment FROM `tutor_rating` LEFT JOIN `users` ON `users`.`id` = student_id WHERE tutor_id = %d", $id);
        if(DB::count() == 0)
            return array();
        return $results;
    }

    function getInterest(){ // get subject list with availability flag by user_id
        include_once("../private/config.php");
        $results = DB::query("SELECT * FROM survey_interest WHERE id=%i", $_SESSION['user_id']);
        if(DB::count()==0){
            return array(
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
            );
        }
        return array(
            $results[0]['sports'] == true,
            $results[0]['music'] == true,
            $results[0]['video_games'] == true,
            $results[0]['dramas_movies'] == true,
            $results[0]['art'] == true,
            $results[0]['books'] == true,
            $results[0]['language'] == true,
            $results[0]['travel'] == true,
            $results[0]['shopping'] == true
        );
    }

    function getOrganizationById($id){
        include_once("../private/config.php");
        $results = DB::query("SELECT * FROM organization WHERE user_id = %i", $id);
        if(DB::count() == 0) return json_decode('{"english_name":"","chinese_name":"","email":"","telephone":"","address":"","website":"","description":""}', true);
        return $results[0];
    }
?>