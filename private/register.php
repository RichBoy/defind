<?php
    // SAVE SESSION
    include("config.php");
    include("passwordFunction.php");
    require_once('../lib/recaptchalib.php');
    header('Content-Type: application/json');
    
    if(!isset($_SESSION)){
        session_start();
    }
    // Get a key from https://www.google.com/recaptcha/admin/create
    $publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
    $privatekey = "6LdDYt8SAAAAAJQ81pyNetMnTVaNLYEYrJws1xaN";

    # the response from reCAPTCHA
    $resp = null;
    # the error code from reCAPTCHA, if any
    $error = null;

    if($_SERVER["REQUEST_METHOD"] == "POST"){

        $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);


        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            $arr['result'] = "failed";
            $arr['errorMsg'] = "wrong captcah: ".$resp->error;
            //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again."."(reCAPTCHA said: " . $resp->error . ")");
        } else {
            $date = new DateTime();
            $encPW = encryptPassword($_POST['password']);
            $email = $_POST['email'];
            $generatedKey = md5(mt_rand(10000,99999).time().$email);
            $updateUserArr = array(
                'email' => $email,
                'username' => $_POST['username'],
                'password' => $encPW,
                'usertype' => $_POST['userType'],
                'createtime' => $date->getTimestamp(),
                // 'status' => 1,
                // 'activekey' => $generatedKey
                'status' => 2,
                'activekey' => 'Activated'
            );
            // Remove activation procedure 2013 08 17
            DB::insert('users', $updateUserArr);
            $arr['result'] = 'success';
            // sendRegMail($email, $generatedKey);
        }
        echo json_encode($arr);
    }

function sendRegMail($email, $key){
    $message = " Hi ".$_POST['username'].",\n\n";
    $message .= "Thank you for your registration. To activate your account on Defind, please click the following link:\n\n";
    $message .= 'www.defind.com.hk/pages/activate.php?email=' . urlencode($email) . "&key=$key";

    if(mail($email, 'Defind Registration Confirmation', $message, 'From:info@defind.com.hk')){
        $arr['result'] = 'success';
    } else {
        $arr['result'] = 'failed';
        $arr['errorMsg'] = 'Cannot send email.';
    }

    echo json_encode($arr);
}

?>