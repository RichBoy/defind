# modification on database

UPDATE `tutor_listing_subjects` tls SET tls.`user_id` = (SELECT `tutor_listings`.`user_id` FROM `tutor_listings` WHERE `tutor_listings`.`id` = `listing_id`);
ALTER TABLE `tutor_listing_subjects` DROP `listing_id`;
ALTER TABLE `tutor_listing_subjects` ADD UNIQUE KEY `new_unique` (`subject_id`, `user_id`);
ALTER TABLE `tutor_listing_subjects` DROP `id`;
ALTER TABLE `tutor_listing_subjects` ADD PRIMARY KEY (`subject_id`, `user_id`);

#TODO:
htaccess for every links in header and footer
Pagination
Registration


INSERT INTO `tutor_details` (`user_id`, `description`, `price_range`)
SELECT user_id, GROUP_CONCAT(details), 1 FROM tutor_listings 
GROUP BY user_id HAVING AVG(price)<100;

INSERT INTO `tutor_details` (`user_id`, `description`, `price_range`)
SELECT user_id, GROUP_CONCAT(details), 2 FROM tutor_listings 
GROUP BY user_id HAVING AVG(price)>=100 AND AVG(price) < 150;

INSERT INTO `tutor_details` (`user_id`, `description`, `price_range`)
SELECT user_id, GROUP_CONCAT(details), 3 FROM tutor_listings 
GROUP BY user_id HAVING AVG(price)>=150 AND AVG(price) < 200;

INSERT INTO `tutor_details` (`user_id`, `description`, `price_range`)
SELECT user_id, GROUP_CONCAT(details), 4 FROM tutor_listings 
GROUP BY user_id HAVING AVG(price)>=200;

