<?php
    include 'header.php';
    $profileText = getLangJSON('../data/profile.json');
    $pmText = getLangJSON('../data/pm_rate.json');
    $tutor_id = $_GET['user'];
?>
<script>
    var RecaptchaOptions = {
        theme : 'clean'
    };
</script>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h2 class="media-heading"><?=$profileText['TITLE']?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="span8 offset2">
                            <h2><?=$profileText['PROFILE']['TITLE']?></h2>
                            <div class="row">
                                <div id="profilePicContainer" class="span3">
                                <?
                                    $fileName = "profile_".$tutor_id;
                                    $fullPath = '../attachments/avatar/'.$fileName.'.jpg';
                                    if(!file_exists($fullPath)){
                                        $fullPath = str_replace($fileName, "default", $fullPath);
                                    }
                                ?>
                                    <img id="profilePic" alt="avatar" class="img-rounded" src="<?=$fullPath?>?key=<?=date("ymdHi")?>"/>
                                </div>
                                <div class="span5">
                                    <div class="well"><?=getProfileDescription($tutor_id)?></div>
                                </div>
                                <!-- <p class="lead">IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?</p> -->
                            </div>
                            <hr>
                                <? $profile = getProfile($tutor_id);?>
                            <div class="row">
                                <div class="span4"><span style="color: #f90;"><?=$profileText['PROFILE']['NAME']?>:</span> <?=$profile['lastname']?> <?=$profile['firstname']?></div>
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['GENDER']?>:</span> <?=($profile['gender'] == ""? "N/A": ($profile['gender'] == 1? 'M':'F'))?></div>
                                <div class="span2"><span style="color: #f90;">GPA:</span> <?=($profile['gpa'] == 0? "N/A": $profile['gpa'])?></div>
                            </div>
                            <div class="row">
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['UNIVERSITY']?>:</span> <?=getUniversityByName($profile['university'])?></div>
                                <? $facultyList = json_decode(file_get_contents("../data/facultyList.json"), true); ?>
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['FACULTY']?>:</span> <?=$facultyList[$profile['faculty_id']]?></div>
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['BIRTH']?>:</span> <?=$profile['birth_year']?></div>
                            </div>
                            <hr>
                            <table id="subjectTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th><?=$profileText['SUBJECT']['TYPE']?></th>
                                        <th><?=$profileText['SUBJECT']['SUBJECTS']?></th>
                                    </tr>
                                </thead>
                                <tbody>
                            <?
                                $subjectList = simplify(getSubjectList($tutor_id));
                                if(count($subjectList) == 0){
                            ?>
                                    <tr><td></td><td><?=$profileText['NO_RESULT']?></td></tr>
                            <?
                                }
                                foreach($subjectList as $typeKey => $typeData){
                            ?>
                                <tr data-type="<?=$typeData->{'typeId'}?>">
                                    <td><?=getStudyTypeID($typeData['typeId'])?></td>
                                    <td>
                                <?
                                    foreach($typeData['subjects'] as $item){
                                ?>
                                        <button type="button" class="subjectBtn btn btn-info" data-subject-id="<?=$item['id']?>"><?=getSubjectByID($item['id'])?></button>
                                <?
                                    }
                                ?>
                                    </td>
                                </tr>
                            <?
                                }
                            ?>
                                </tbody>
                            </table>
                    <?
                        $rateList = getAvgRate($tutor_id);
                        if(count($rateList) > 0){
                            $overall = round(($rateList['avgAttitude'] + $rateList['avgClarity'] + $rateList['avgKnowledge'] + $rateList['avgPreparedness']) / 4);
                    ?>
                            <table id="rateTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?=$pmText['TOTALRATE']?></th>
                                        <th><?=$pmText['ATTITUDE']?></th>
                                        <th><?=$pmText['CLARITY']?></th>
                                        <th><?=$pmText['KNOWLEDGE']?></th>
                                        <th><?=$pmText['PREPAREDNESS']?></th>
                                        <th><?=$pmText['OVERALL']?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href='viewComment.php?user=<?=$tutor_id?>'><?=$rateList['total']?></a></td>
                                        <td><span class="label"><?=$rateList['avgAttitude']?></span></td>
                                        <td><span class="label"><?=$rateList['avgClarity']?></span></td>
                                        <td><span class="label"><?=$rateList['avgKnowledge']?></span></td>
                                        <td><span class="label"><?=$rateList['avgPreparedness']?></span></td>
                                        <td><span class="label"><?=$overall?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                    <?
                        }
                    ?>
                        </div>
                    </div>
                <? if(!isset($_SESSION['usertype']) || $_SESSION['usertype'] == "student"){ ?>
                    <div class="row">
                        <div class="span6 offset2">
                            <?
                                $messageFormTargetId = $tutor_id;
                                include 'messageForm.php'
                            ?>
                        </div>
                    </div>
                <? } ?>
<?php
    include 'footer.php';
?>

<script>
    function addSubjectToJson(){
        var obj = {};
        _.each($('.subjectBtn.active'), function(item){
            // console.log($(item).parents('tr').data('type'));
            // console.log($(item).text());
        });
        return jsonStr;
    }

    // called when dom elements are ready
    $(function() {
        $('#rateTable .label').each(function(){
            var point = $(this).text();
            if(point < 50) $(this).addClass('label-important');
            else if(point < 80) $(this).addClass('label-warning');
            else $(this).addClass('label-success');
        });
    });
</script>