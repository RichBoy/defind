<?php
    include 'header.php';
    $profileText = getLangJSON('../data/profile.json');
    function getAllOrganizations(){
        include_once("../private/config.php");
        // $results = DB::query("SELECT * FROM organization");
        $results = DB::query("SELECT o.*, u.`status`\n"
            . "FROM `organization` o \n"
            . "LEFT OUTER JOIN `users` u ON u.id = o.user_id \n"
            . "GROUP BY u.id ORDER BY o.english_name");
        return $results;
    }
    $organizations = getAllOrganizations();
?>

                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading"><?=$profileText['ORG_TITLE']?></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="span8 offset2">
                        <h2><?=$profileText['ORG']['TITLE_LIST']?></h2>
                        <br />
                        <? foreach($organizations as $index => $organization){ 
                            if($organization['status']!=3){ ?>
                            <div class="tabbable tabs-left">
                                <h4 class="media-heading"><?=$organization['english_name']?>
                                    <? if(isset($login_session) and $_SESSION['usertype'] == 'admin'){ ?>
                                        <a href="organizationUser.php?uid=<?=$organization['user_id']?>" class="icon-pencil" style="margin: 8px 0 0 8px;"></a>
                                        <a href="#confirmModal" role="button" class="icon-trash" data-toggle="modal" data-delid="<?=$organization['user_id']?>" style="margin: 8px 0 0 8px;"></a>
                                    <? } else if(isset($login_session)){ ?>
                                        <a href="pm.php?id=<?=$organization['user_id']?>" class="icon-envelope" style="margin: 8px 0 0 8px;"></a>
                                    <? } ?>
                                </h4>
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1_<?=$organization['user_id']?>" data-toggle="tab"><?=$profileText['ORG']['INFO']?></a></li>
                                    <li><a href="#tab2_<?=$organization['user_id']?>" data-toggle="tab"><?=$profileText['ORG']['DESCRIPTION']?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1_<?=$organization['user_id']?>">
                                        <div class="row-fluid">
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['ENGLISH_NAME']?></span>: <?=$organization['english_name']?></div>
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['CHINESE_NAME']?></span>: <?=$organization['chinese_name']?></div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['TELEPHONE']?></span>: <?=$organization['telephone']?></div>
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['EMAIL']?></span>: <?=$organization['email']?></div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['ADDRESS']?></span>: <?=$organization['address']?></div>
                                            <div class="span6"><span class="label label-warning"><?=$profileText['ORG']['WEBSITE']?></span>: <?=$organization['website']?></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2_<?=$organization['user_id']?>">
                                        <p><?=nl2br($organization['description'])?></p>
                                    </div>
                                </div>
                            </div>
                            <br />
                        <? }
                        } ?>
                    </div>
                    <div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="confirmLabel">Confirmation</h3>
                        </div>
                        <div class="modal-body">
                            <p id="delConfContact">Are you sure you want to delete this Org?</p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                            <button id="delOrgConfBtn" class="btn btn-primary">Yes</button>
                            <span id="updateTutorRes" class="inline"></span>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        var delId;
        $('.icon-trash').click(function(e){
            delId = $(this).data('delid');
        });

        $('#delOrgConfBtn').click(function(e){
            e.preventDefault();
            var data = {
                inputUserId : delId,
                inputAction : 'delete'
            },
            result = $('#delConfContact');
            result.addClass('muted');
            result.text('loading...');

            $.ajax({
                url: '../data/updateOrg.php',
                type: 'POST',
                data: data,
                success: function(response) {
                    if(response == "success"){
                        $('#delOrgConfBtn').attr('disabled','disabled');
                        result.addClass('text-success');
                        result.text('Update success');
                    } else {
                        result.addClass('text-error');
                        result.text('Update failed')
                    }
                }
            });
        });
    });
</script>