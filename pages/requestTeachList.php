<?php
    include 'header.php';
    if(!isset($login_session) || $_SESSION['usertype'] != "tutor"){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
    $acceptedOnly = 0;
    if(isset($_GET['acceptedOnly'])){
        $acceptedOnly = 1;
    }
    $pmText = getLangJSON('../data/pm_rate.json');
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="pm.php"><?=$pmText['RECEIVE']?></a>
                        </li>
                        <li class="">
                            <a href="pm.php?sendOnly"><?=$pmText['SENT']?></a>
                        </li>
                        <li class="<?=($acceptedOnly?'':'active')?>">
                            <a href="requestTeachList.php"><?=$pmText['REQUEST']?></a>
                        </li>
                        <li class="<?=($acceptedOnly?'active':'')?>">
                            <a href="requestTeachList.php?acceptedOnly"><?=$pmText['ACCEPTED']?></a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <table id="requestTable" class="table table-hover span10 offset1">
                        <thead>
                            <tr>
                                <th class="span5"><?=$pmText['STUDENT']?></th>
                                <th class="span3"><?=$pmText['DATE']?></th>
                                <th class="span2"><?=($acceptedOnly?$pmText['ACCEPTED']:$pmText['ACCEPT'])?></th>
                            </tr>
                        </thead>
                        <tbody>
                    <? 
                        include_once('../data/requestTeach.php');
                        if($acceptedOnly){
                            $requests = getAcceptedRequest($_SESSION['user_id']);
                        } else {
                            $requests = getRequest($_SESSION['user_id']);
                        }
                        foreach($requests as $index => $request){
                    ?>
                            <tr>
                                <td><?=$request['username']?></td>
                                <td><?=$request['date']?></td>
                        <?  if($acceptedOnly){ ?>
                                <td>Accepted</td>
                        <?  } else {?>
                                <td><button data-student="<?=$request['student_id']?>" class="btn btn-primary acceptBtn"><?=$pmText['ACCEPT']?></button></td>
                        <?  } ?>
                            </tr>
                    <?
                        }
                    ?>
                        </tbody>
                    </table>
                </div>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        $('#requestTable td .acceptBtn').click(function(e){
            e.preventDefault();
            // console.log($(this).data('student'));
            $(this).attr('disabled','disabled');
            var data = {
                student: $(this).data('student'),
                action: 'acceptRequest'
            };
            $.ajax({
                url: '../private/requestTeach.php',
                type: 'POST',
                data: data,
                success: function(response) {
                    location.reload();
                }
            });
        });
    });
</script>