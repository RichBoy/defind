<?php
    include 'header.php';
    $target = 'privacy';

    $privacy = getLangJSON('../data/'.$target.'.json');
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <h4><?echo $privacy['TITLE']?></h4>
                    </div>
                    <div class="span9">
                        <div class="row">
                            <p class="lead sub-title"><?=$privacy['TITLE']?></p>
                            <hr>
                            <p class="content"><strong><?=$privacy['SUBCONTENT']?></strong></p>
                        </div>
                        <br />
                        <?
                            // Show all content in the json
                            foreach($privacy['CONTENT'] as $item){
                                $subtitle = $item['SUBTITLE'];
                                $subcontent = $item['SUBCONTENT'];
                        ?>
                                <div class="row">
                                    <p class="sub-title"><?=$subtitle?></p>
                                    <p class="content"><?=$subcontent?></p>
                                </div>
                                <br />
                        <?
                            }
                        ?>
                        <br />
                        <div class="row">
                            <p class="content"><strong><?=$privacy['CONTACTUS']?></strong></p>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
</script>