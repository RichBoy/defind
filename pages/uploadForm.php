<?php
    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($_SESSION['login_user'])){
        return;
    }
    $fileName = "profile_".$_SESSION['user_id'];
    $fullPath = '../attachments/avatar/'.$fileName.'.jpg';

    if(isset($_POST['submit']) ) {
        include('../lib/simpleImage.php');
        $image = new SimpleImage();

        $image->load($_FILES['uploaded_image']['tmp_name']);
        $image->resizeToWidth(150);
        if($image->getHeight() > 300){
            $image->resizeToHeight(300);
        }
        $image->save($fullPath);
    }

    if(!file_exists($fullPath)){
        $fullPath = str_replace($fileName, "default", $fullPath);
    }
?>
<center>
    <img src="<?=$fullPath?>?key=<?=date("ymdHi")?>" />

    <form action="uploadForm.php" method="post" enctype="multipart/form-data">
        <input type="file" accept="image/*" name="uploaded_image" /><br />
        <input type="submit" name="submit" value="Upload" />
    </form>
</center>