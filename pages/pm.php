<?php
    include 'header.php';
    if(!isset($login_session)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
    $page = 0;
    if(isset($_GET['page'])){
        $page = $_GET['page'];
    }
    $senderId = -1;
    if(isset($_GET['id'])){
        $senderId = $_GET['id'];
    }

    $sendOnly = 0;
    if(isset($_GET['sendOnly'])){
        $sendOnly = 1;
    }

    $pmText = getLangJSON('../data/pm_rate.json');
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
            <?
                if($senderId > 0){
            ?>
                    <div class="row">
                        <div class="span6 offset2">
                            <?
                                $messageFormTargetId = $senderId;
                                include 'messageForm.php';
                            ?>
                    <?
                        if($_SESSION['usertype'] == "student"){
                    ?>
                            <center>
                                <button id="requestTeach" class="btn btn-primary" disabled='disabled'>Request Teaching</button>
                            </center>
                    <?
                        }
                    ?>
                        </div>
                    </div>
            <?
                } else {
            ?>
                    <ul class="nav nav-tabs">
                        <li class="<?=(!$sendOnly?'active':'')?>">
                            <a href="pm.php"><?=$pmText['RECEIVE']?></a>
                        </li>
                        <li class="<?=($sendOnly?'active':'')?>">
                            <a href="pm.php?sendOnly"><?=$pmText['SENT']?></a>
                        </li>
                <?  if($_SESSION['usertype'] == "tutor"){?>
                        <li class="">
                            <a href="requestTeachList.php"><?=$pmText['REQUEST']?></a>
                        </li>
                        <li class="">
                            <a href="requestTeachList.php?acceptedOnly"><?=$pmText['ACCEPTED']?></a>
                        </li>
                <?  } else if($_SESSION['usertype'] == "student"){ ?>
                        <li class="">
                            <a href="myTutor.php"><?=$pmText['MYTUTOR']?></a>
                        </li>
                        <li class="">
                            <a href="myTutor.php?ratedOnly"><?=$pmText['RATEDTUTOR']?></a>
                        </li>
                <?  } ?>
                    </ul>
            <?
                }
            ?>
                    <table id="pmTable" class="table table-hover span10 offset1">
                        <thead>
                            <tr>
                                <th class="span5"><?=$pmText['MESSAGE']?></th>
                                <th class="span2"><?=($sendOnly?$pmText['TO']:$pmText['FROM'])?></th>
                                <th class="span3"><?=$pmText['TIME']?></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="pagination pagination-centered">
                        <ul>
                        </ul>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
    var currentPage = 0;
    function updatePaging(totalPage){
        var paging = $('.pagination ul');
        paging.empty();

        var addPage = function(text, className, page){
            var li = $(document.createElement('li'));
            var a = $(document.createElement('a'));

            if(className) li.attr({"class": className});
            else a.attr({"onClick": "updateResult(" + page + ")"});
            a.attr({"href": "#"});
            a.html(text);
            li.append(a);
            this.append(li);
        };

        _.bind(addPage, paging)("&laquo;", (currentPage > 0 ? "": "disabled"), currentPage - 1);
        _(totalPage).times(function(index){
            var pageNo = index + 1;
            var className = "";
            if(index == currentPage) className = "active";
            _.bind(addPage, paging)(pageNo, className, index);
        });
        _.bind(addPage, paging)("&raquo;", (currentPage + 1 < totalPage? "": "disabled"), currentPage + 1);
    }

    function rowEvent(){
        $('#pmTable tbody tr td.pm-content').click(function(){
            if(!$(this).data('expand')){
                $('#pmTable tbody tr td.pm-content:data(expand)').each(function(){
                    $(this).data('expand', false);
                    $(this).text($(this).data('short'));
                });
                $(this).data('expand', true);
                $(this).text($(this).data('content'));
                <?if(!$sendOnly){?>
                    $(this).append('<br/><a href="pm.php?id='+$(this).data('sender')+'" class="btn btn-info"><?=$pmText["REPLY"]?></a>');
                <?}?>
            } else {
                $(this).data('expand', false);
                $(this).text($(this).data('short'));
            }
        });
    }

    function createRow(data){
        var newRow = function(){
            return $(document.createElement('tr'));
        };
        var content = function(senderId, message){
            var td = $(document.createElement('td'));
            td.attr({"class": "pm-content"});
            td.data("sender", senderId);
            td.data("content", message);

            var shortContent = message.substr(0, 40) + (message.length > 40? "..." : "");
            td.data("short", shortContent);
            td.text(shortContent);
            return td;
        }
        var name = function(id, name){
            var td = $(document.createElement('td'));
            td.attr({"class": "pm-name"});

            var a = $(document.createElement('a'));
            a.attr({"href": "pm.php?id=" + id});
            a.text(name);
            td.append(a);
            return td;
        }
        var date = function(time){
            var td = $(document.createElement('td'));
            td.attr({"class": "pm-date"});
            td.text(time);
            return td;
        }
        var row = newRow();
        row.append(content(data.sender_id, data.message));
        row.append(name(data.<?=($sendOnly?'receiver':'sender')?>_id, data.<?=($sendOnly?'receiver':'sender')?>_name));
        row.append(date(data.send_time));
        return row;
    }

    function appendTable(response){
        // console.log(response.count);
        updatePaging(response.count);
        // pagination

        var tbody = $('#pmTable tbody');
        tbody.empty();
        _.each(response.pm, function(item){
            tbody.append(createRow(item));
        });
        rowEvent();
    }

    function updateResult(page){
        var data = {
            page: page,
            target_id: <?=$senderId?>,
            sendOnly: <?=$sendOnly?>
        };
        $.ajax({
            url: '../data/pm.php',
            type: 'POST',
            data: data,
            success: function(response) {
                currentPage = page;
                appendTable(response);
                // console.log(response);
            }
        });
    }

    function checkRequested(){
        var data = {
            tutor: <?=$senderId?>,
            action: 'checkRequested'
        };
        $.ajax({
            url: '../private/requestTeach.php',
            type: 'POST',
            data: data,
            success: function(response) {
                if(response.result == 'notExist'){
                    $('#requestTeach').removeAttr('disabled');
                    $('#requestTeach').click(function(e){
                        e.preventDefault();
                        sendRequest();
                    });
                }
            }
        });
    }

    function sendRequest(){
        $('#requestTeach').attr('disabled','disabled');
        var data = {
            tutor: <?=$senderId?>,
            action: 'sendRequest'
        };
        $.ajax({
            url: '../private/requestTeach.php',
            type: 'POST',
            data: data,
            success: function(response) {
                if(response.result != 'success'){
                    $('#requestTeach').removeAttr('disabled');
                }
            }
        });
    }

    // called when dom elements are ready
    $(function() {
        updateResult(currentPage);
    <?  if($_SESSION['usertype'] == "student"){?>
        checkRequested();
    <?  } ?>
    });
</script>