<?php
    include 'header.php';
    $profileText = getLangJSON('../data/profile.json');
    if(!isset($login_session)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }

    if($_SESSION['usertype'] == 'org'){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h2 class="media-heading"><?=$profileText['TITLE']?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="span8 offset2">
                        <h2><?=$profileText['PROFILE']['TITLE']?></h2>
                        <div class="row">
                            <div id="profilePicContainer" class="span3">
                                <iframe src="uploadForm.php" frameborder="0" height="340" seamless></iframe>
                            </div>
                            <? $tutorDetail = getTutorDetail($_SESSION['user_id']);?>
                            <div class="span4">
                                <textarea id="profileDescription" rows="6" placeholder="<?=$profileText['PROFILE']['DESCRIPTION']?>"><?=$tutorDetail['description']?></textarea>
                            </div>
                        </div>

                        <?
                            $profile = getProfile($_SESSION['user_id']);
                            //var_dump($profile);
                            $userId = $_SESSION['user_id'];
                            if(!isset($profile['lastname'])) $profile['lastname'] = "";
                            if(!isset($profile['firstname'])) $profile['firstname'] = "";
                            if(!isset($profile['availability'])) $profile['availability'] = "1";
                            if(!isset($profile['birth_year'])) $profile['birth_year'] = "";
                            if(!isset($profile['university_id'])) $profile['university_id'] = "0";
                            if(!isset($profile['faculty_id'])) $profile['faculty_id'] = "0";
                            if(!isset($profile['gender'])) $profile['gender'] = "1";
                            if(!isset($profile['region_id'])) $profile['region_id'] = "0";
                            if(!isset($profile['chinese_name'])) $profile['chinese_name'] = "";
                            if(!isset($profile['district_ids'])) $profile['district_ids'] = "";
                            if(!isset($profile['school'])) $profile['school'] = "";
                        ?>

                        <form id="updateProfileForm" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="inputLastName"><?=$profileText['PROFILE']['LAST_NAME']?></label>
                                <div class="controls">
                                    <input type="text" id="inputLastName" pattern="[a-zA-Z ]{1,20}" required placeholder="<?=$profileText['PROFILE']['LAST_NAME']?>" value="<?=$profile['lastname']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputFirstName"><?=$profileText['PROFILE']['FIRST_NAME']?></label>
                                <div class="controls">
                                    <input type="text" id="inputFirstName" pattern="[a-zA-Z ]{1,20}" required placeholder="<?=$profileText['PROFILE']['FIRST_NAME']?>" value="<?=$profile['firstname']?>"/>

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputChiName"><?=$profileText['PROFILE']['CHINESE_NAME']?></label>
                                <div class="controls">
                                    <input type="text" id="inputChiName" placeholder="<?=$profileText['PROFILE']['CHINESE_NAME']?>" value="<?=$profile['chinese_name']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputSex"><?=$profileText['PROFILE']['SEX']?></label>
                                <div class="controls">
                                    <select id="inputSex">
                                        <option value="1"<?=($profile['gender']=="1"?" selected":"")?>>M</option>
                                        <option value="2"<?=($profile['gender']=="2"?" selected":"")?>>F</option>
                                    </select>
                                </div>
                            </div>
                    <?  if($_SESSION['usertype'] == "tutor"){ ?>
                            <div class="control-group">
                                <label class="control-label" for="inputUniversity"><?=$profileText['PROFILE']['UNIVERSITY']?></label>
                                <div class="controls">
                                    <select id="inputUniversity">
                                    <?
                                        $universities = getLangJSON("../data/universityList.json");
                                        foreach($universities as $index => $university){
                                            if($index == 0) continue;
                                            $selected = false;
                                            if($index == $profile['university_id']) $selected = true;
                                            else if($index == 8 && $profile['university_id'] == 0) $selected = true;
                                    ?>
                                        <option value="<?=$index?>"<?=($selected?" selected":"")?>><?=$university?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputFaculty"><?=$profileText['PROFILE']['FACULTY']?></label>
                                <div class="controls">
                                    <select id="inputFaculty">
                                    <?
                                        $faculties = getJSON("../data/facultyList.json");
                                        foreach($faculties as $index => $faculty){
                                            if($index == 0) continue;
                                            $selected = false;
                                            if($index == $profile['faculty_id']) $selected = true;
                                            else if($index == 12 && $profile['faculty_id'] == 0) $selected = true;
                                    ?>
                                        <option value="<?=$index?>"<?=($selected?" selected":"")?>><?=$faculty?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputGpa">GPA</label>
                                <div class="controls">
                                    <input type="number" min="0.0" max="4.5" step="0.1" id="inputGpa" value="<?=($profile['gpa']>0?$profile['gpa']:'')?>"/>
                                </div>
                            </div>
                    <?  } else if($_SESSION['usertype'] == "student"){ ?>
                            <div class="control-group">
                                <label class="control-label" for="inputSchool"><?=$profileText['PROFILE']['SCHOOL']?></label>
                                <div class="controls">
                                    <input type="text" id="inputSchool" placeholder="<?=$profileText['PROFILE']['SCHOOL']?>" value="<?=$profile['school']?>"/>
                                </div>
                            </div>
                    <?  } ?>
                            <div class="control-group">
                                <label class="control-label" for="inputBirth"><?=$profileText['PROFILE']['BIRTH']?></label>
                                <div class="controls">
                                    <input type="number" min="1950" max="2000" id="inputBirth" placeholder="<?=$profileText['PROFILE']['BIRTH']?>" value="<?=$profile['birth_year']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputRegion"><?=$profileText['PROFILE']['REGION']?></label>
                                <div class="controls">
                                    <select id="inputRegion">
                                    <?
                                        $regions = getLangJSON("../data/regionList.json");
                                        foreach($regions as $index => $region){
                                    ?>
                                        <option value="<?=$index?>"<?=($index == $profile['region_id']?" selected":"")?>><?=$region?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputDistrict"><?=$profileText['PROFILE']['DISTRICTS']?></label>
                                <div id="inputDistrict" class="controls" data-districts="<?=$profile['district_ids']?>">
                                    <?
                                        $districts = getDistrictsByRegion($profile['region_id']);
                                        $districtsArr = array();
                                        if($profile['district_ids'] != ''){
                                            $districtsArr = explode(",", $profile['district_ids']);
                                        }
                                        foreach ($districts as $key => $value) {
                                    ?>
                                            <button type="button" class="btn btn-info districtBtn <?=in_array($value['id'], $districtsArr)? 'active':''?>" data-district-id="<?=$value['id']?>" data-toggle="button"><?=$value['name']?></button>
                                    <?
                                        }
                                    ?>
                                </div>
                            </div>
                    <?  if($_SESSION['usertype'] == "tutor"){ ?>
                            <div class="control-group">
                                <label class="control-label" for="inputPrice"><?=$profileText['PROFILE']['PRICE']?></label>
                                <div class="controls">
                                    <select id="inputPrice">
                                    <?
                                        $prices = getJSON("../data/priceRange.json");
                                        foreach($prices as $index => $price){
                                    ?>
                                        <option value="<?=$index+1?>" <?=($index + 1 == $tutorDetail['price_range']?" selected":"")?>><?=$price?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="control-group">
                                <label class="control-label" for="resumeUpload">Upload</label>
                                <div class="controls">
                                    <input id="resumeUpload" type="file" name="uploaded_resume" />?
                                </div>
                            </div> -->
                    <?  } ?>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <input type="hidden" id="inputUserId" value="<?=$userId?>" />
                                    <input type="hidden" id="inputAction" value="<?='updatePro'?>" />
                                    <button type="submit" class="btn"><?=$profileText['UPDATE']?></button>
                                    <span id="updateProfileRes" class="inline"></span>
                                </div>
                            </div>
                        </form>
                        <br />
                        <form id="updatePwForm" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="inputOldPw"><?=$profileText['RESET_PW']['PRESENT_PW']?></label>
                                <div class="controls">
                                    <input class="password" id="inputOldPw" type="password" required placeholder="<?=$profileText['RESET_PW']['PRESENT_PW']?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputNewPw"><?=$profileText['RESET_PW']['NEW_PW']?></label>
                                <div class="controls">
                                    <input class="password" id="inputNewPw" type="password" required placeholder="<?=$profileText['RESET_PW']['NEW_PW']?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputNewPw2"><?=$profileText['RESET_PW']['VERFY_NEW_PW']?></label>
                                <div class="controls">
                                    <input class="password" id="inputNewPw2" type="password" required placeholder="<?=$profileText['RESET_PW']['VERFY_NEW_PW']?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <input type="hidden" id="inputUserId" value="<?=$userId?>" />
                                    <input type="hidden" id="inputAction" value="<?='changePw'?>" />
                                    <button type="submit" class="btn"><?=$profileText['UPDATE']?></button>
                                    <span id="updatePwRes" class="inline"></span>
                                </div>
                            </div>
                        </form>
                <?
                    if($_SESSION['usertype'] == 'tutor'){
                ?>
                        <hr>
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?=$profileText['SUBJECT']['REMINDER']?>
                        </div>
                        <table id="subjectTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><?=$profileText['SUBJECT']['TYPE']?></th>
                                    <th><?=$profileText['SUBJECT']['SUBJECTS']?></th>
                                </tr>
                            </thead>
                            <tbody>
                        <?
                            $subjectList = simplify(getSubjectList());
                            foreach($subjectList as $typeKey => $typeData){
                        ?>
                            <tr data-type="<?=$typeData['typeId']?>">
                                <td><?=getStudyTypeID($typeData['typeId'])?></td>
                                <td>
                            <?
                                foreach($typeData['subjects'] as $item){
                            ?>
                                    <button type="button" class="subjectBtn btn btn-info<?=($item['available']?" active":"")?>" data-subject-id="<?=$item['id']?>" data-toggle="button"><?=getSubjectByID($item['id'])?></button>
                            <?
                                }
                            ?>
                                </td>
                            </tr>
                        <?
                            }
                        ?>
                            </tbody>
                        </table>
                        <center><button id="updateBtn" class="btn btn-large btn-primary" type="button"><?=$profileText['UPDATE']?></button></center>
                <?
                    } else {
                        $surveyText = getLangJSON('../data/interestSurvey.json');
                        $interestList = getInterest();
                ?>
                        <hr>
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <?=$surveyText['TITLE']?>
                    </div>
                        <div id="interestContainer" class="row">
                    <?  foreach($surveyText['OPTION'] as $index => $title){ ?>
                            <button type="button" class="interestBtn btn btn-info<?=($interestList[$index]?' active':'')?>" data-interest="<?=$index?>" data-toggle="button"><?=$title?></button>
                    <?  } ?>
                        </div>
                    <center><button id="updateBtn" class="btn btn-large btn-primary" type="button"><?=$profileText['UPDATE']?></button></center>
                <?
                    }
                ?>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
    var districtArr = [],
        districts = '';
    function printDistrictBtn(rid){
        if(!rid || $.isEmptyObject(districtArr)) return;
        var parent = $("#inputDistrict"),
            districtIds = districts.length > 0? districts : parent.data('districts'),
            districtIdsArr = [];
        districts = districtIds;

        if(districtIds && typeof districtIds == "string"){
            districtIdsArr = districtIds.split(',');
        } else {
            districtIdsArr[0] = districtIds;
        }
        parent.empty();

        _.each(districtArr[rid], function(item){
            var btnClass = "btn btn-info districtBtn";
            if($.inArray(item.id.toString(), districtIdsArr) != -1){
                btnClass += " active";
            }
            var btn = $('<button/>',
            {
                text: item.name,
                class: btnClass
            }).attr({
                'data-district-id':item.id,
                'data-toggle':'button',
                'type':'button'
            });
            parent.append(btn);
        });
    }
    // called when dom elements are ready
    $(function() {
    <?if($_SESSION['usertype'] == 'tutor'){?>
        districtArr = $.parseJSON('<?=json_encode(getLangJSON("../data/districts.json"))?>');

        $('#updateBtn').attr('disabled','disabled');
        // $('#profileDescription').change(function(e){
        //     $('#updateBtn').removeAttr('disabled');
        // });
        $('.subjectBtn').click(function(e){
            $('#updateBtn').removeAttr('disabled');
        });
        $('#updateBtn').click(function(e){
            e.preventDefault();
            var subjectArray = [];
            $('.subjectBtn.active').each(
                function(){
                    subjectArray.push($(this).data('subject-id'));
                }
            );

            $.ajax({
                url: '../data/updateSubject.php',
                type: 'POST',
                data: {
                    subjects: JSON.stringify(subjectArray)
                },
                success: function(response) {
                    if(response == "success"){
                        alert("<?=$profileText['REMINDER']['SUCCESS']?>");
                    } else {
                        alert("<?=$profileText['REMINDER']['FAIL']?>");
                    }
                }
            });
            $(this).attr('disabled','disabled')
        });
        $('#inputRegion').change(function(e){
            e.preventDefault();
            var rid = $(this).val();
            printDistrictBtn(rid);
        });
    <?} else if($_SESSION['usertype'] == 'student'){ ?>
        $('#updateBtn').attr('disabled','disabled');
        $('.interestBtn').click(function(e){
            $('#updateBtn').removeAttr('disabled');
        });
        $('#updateBtn').click(function(e){
            e.preventDefault();
            // var interestArray = [];
            // $('.interestBtn.active').each(
            //     function(){
            //         interestArray.push($(this).data('interest-id'));
            //     }
            // );
            $.ajax({
                url: '../data/updateInterest.php',
                type: 'POST',
                data: {
                    sports: $('[data-interest=0]').hasClass('active'),
                    music: $('[data-interest=1]').hasClass('active'),
                    video_games: $('[data-interest=2]').hasClass('active'),
                    dramas_movies: $('[data-interest=3]').hasClass('active'),
                    art: $('[data-interest=4]').hasClass('active'),
                    books: $('[data-interest=5]').hasClass('active'),
                    language: $('[data-interest=6]').hasClass('active'),
                    travel: $('[data-interest=7]').hasClass('active'),
                    shopping: $('[data-interest=8]').hasClass('active')
                },
                success: function(response){
                    if(response == "success"){
                        alert("<?=$profileText['REMINDER']['SUCCESS']?>");
                    } else {
                        alert("<?=$profileText['REMINDER']['FAIL']?>");
                    }
                }
            });
            $(this).attr('disabled','disabled')
        });
    <?}?>

        $('#updateProfileForm').submit(function(e){
            e.preventDefault();
            var inputArray = {},
                districtArray = [],
                result = $('#updateProfileRes');
            inputArray['description'] = $('#profileDescription').val();
            $('#updateProfileForm input, #updateProfileForm select').each(
                function(){
                    var key = $(this).attr('id');
                    if($(this).is(':radio')){
                        if($(this).is(':checked')){
                            key = $(this).attr('name');
                        } else return;
                    }
                    inputArray[key] = $(this).val();
                }
            );
            $('#updateProfileForm button.districtBtn.active').each(function(){
                districtArray.push($(this).data('district-id'));
            });

            if(districtArray.length > 0){
                inputArray['inputDistrict'] = JSON.stringify(districtArray);
                districts = districtArray.join();
            }
            result.addClass('icon-loading');
            $.ajax({
                url: '../data/updateTutor.php',
                type: 'POST',
                data: inputArray,
                success: function(response) {
                    result.removeClass('icon-loading');
                    if(response == "success"){
                        result.addClass('text-success');
                        result.text("<?=$profileText['REMINDER']['SUCCESS']?>");
                    } else {
                        result.addClass('text-error');
                        result.text("<?=$profileText['REMINDER']['FAIL']?>")
                    }
                }
            });
        });

        $('#updatePwForm').submit(function(e){
            e.preventDefault();
            var inputArray = {},
                result = $('#updatePwRes');
            $('#updatePwForm input').each(
                function(){
                    var key = $(this).attr('id');
                    inputArray[key] = $(this).val();
                }
            );
            result.addClass('icon-loading');
            if(inputArray['inputNewPw'] != inputArray['inputNewPw2']){
                result.removeClass('icon-loading');
                result.addClass('text-error');
                result.text('Cannot verify new password');
                return;
            };
            $.ajax({
                url: '../data/updateTutor.php',
                type: 'POST',
                data: inputArray,
                success: function(response) {
                    result.removeClass('icon-loading');
                    if(response == "success"){
                        result.addClass('text-success');
                        result.text("<?=$profileText['REMINDER']['SUCCESS']?>");
                    } else {
                        result.addClass('text-error');
                        result.text("<?=$profileText['REMINDER']['FAIL']?>")
                    }
                }
            });
        });
    });
</script>