<?php
include 'header.php';
include_once("../private/config.php");

if (isset($_GET['email']) && preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $_GET['email'])){
    $email = $_GET['email'];
}

if (isset($_GET['key']) && (strlen($_GET['key']) == 32)){ //The Activation key will always be 32 since it is MD5 Hash
    $key = $_GET['key'];
}

if (isset($email) && isset($key)) {
    $check = false;
    $results = DB::query("SELECT id,activekey FROM users WHERE email=%s", $email);
    if(DB::count() == 1){
        $res = $results[0];

        if($res['activekey'] == $key){
            $updateUserArr = array(
                'status' => 2,
                'activekey' => 'Activated'
            );
            DB::update('users', $updateUserArr , "email=%s", $email);
            $check = true;
        }
    }
} else {
?>
        <div class="well">
            Error Occured.
        </div>
<?
    return;    
}
$activateText = getLangJSON('../data/activate.json');
?>
<div class="container">
    <div class="alert <?=$check?'alert-success':'alert-error'?>">
        <? if($check){ ?>
            <div><?=$activateText['SUCCESS']?></div>
        <? } else { ?>
            <div><?=$activateText['FAIL']?></div>
        <? } ?>
    </div>
</div>
<?php
    include 'footer.php';
?>