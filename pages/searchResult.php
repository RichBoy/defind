<?php
    include 'header.php';

    if(!isset($_SESSION)){
        session_start();
    }
    $searchParam = $_SESSION['searchParam'];
    $searchText = getLangJSON('../data/searchResult.json');
?>
                <div class="span10 offset1">
                    <!-- Visible to desktop only -->
                    <div class="row visible-desktop">
                        <!--LOGO-->
                        <div class="span3">
                            <img src="../images/logo_subpage.png" />
                        </div>
                        <div class="span7">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?=$searchText['TUTORS_IN']?>:</th>
                                        <th><?=$searchText['TEACHING']?>:</th>
                                        <th><?=$searchText['SUBJECTS']?>:</th>
                                        <th><?=$searchText['AT']?>:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=getUniversityByName($searchParam['university'])?></td>
                                        <td><?=getStudyTypeID($searchParam['study_id'])?></td>
                                        <td><?=getSubjectByID(implode(", ", $searchParam['subjects_id']))?></td>
                                        <td><?=getRegionID($searchParam['region_id'])?></td>
                                </tbody>
                            </table>
                            <p class="text-right">
                                <button class="btn btn-large btn-primary" type="button" onClick="$(location).attr('href', 'index.php');"><?=$searchText['SEARCH_AGAIN']?></button>
                            </p>
                        </div>
                    </div>
                    <div id="filter" class="row">
                        <div class="span2 offset1">
                            <div class="row">
                                <h5><?=$searchText['GENDER']?></h5>
                                <ul id="genderTab" class="nav nav-pills nav-stacked">
                                    <li class="active"><a href="#" data-gender=0><?=$searchText['ALL']?></a></li>
                                    <li><a href="#" data-gender=1><?=$searchText['MALE']?></a></li>
                                    <li><a href="#" data-gender=2><?=$searchText['FEMALE']?></a></li>
                                </ul>
                            </div>
                            <div class="row">
                                <h5><?=$searchText['PRICE']?></h5>
                                <ul id="priceTab" class="nav nav-pills nav-stacked">
                                    <li class="active"><a href="#"><?=$searchText['ALL']?></a></li>
                            <?
                                $priceRange = getJSON('../data/priceRange.json');
                                foreach($priceRange as $key => $value){
                            ?>
                                    <li><a href="#" data-price-range=<?=($key+1)?>><?=$value?></a></li>
                            <? } ?>
                                </ul>
                            </div>
                            <div class="row">
                                <h5><?=$searchText['DISTRICTS']?></h5>
                                <ul id="districtTab" class="nav nav-pills nav-stacked">
                                    <li class="active"><a href="#" data-district-id="0"><?=$searchText['ALL']?></a></li>
                            <?
                                $districts = getDistrictsByRegion($searchParam['region_id']);
                                foreach ($districts as $key => $value) {
                            ?>
                                    <li><a href="#" data-district-id="<?=$value['id']?>"><?=$value['name']?></a></li>
                            <?
                                }
                            ?>
                                </ul>
                            </div>
                        </div>

                        <? // result table start ?>
                        <div id="resultTable" class="span7">
                        </div> 
                    </div>
                </div>
<?
    include 'footer.php';
?>

<script>
    function createRow(id, name, gender, location, price, subjects, description, verified){
        var newRow = function(){
            return $(document.createElement('div')).attr({"class": "row-fluid"});
        };
        var newSpan = function(size, html){
            var span = $(document.createElement('div')).attr({"class": ("span" + size)});
            span.append(html);
            return span;
        }
        id = id || "";
        name = name || "";
        gender = gender || "";
        location = location || "";
        price = price || "";
        subjects = subjects || "";
        description = description || "";
        verified = parseInt(verified) || false;

        var outerDiv = $(document.createElement('div')).attr({"class": "row-fluid tutorRow"});

        var row = newRow();
        row.append("<a href='#' style='color: black'><strong onclick=\'$(location).attr(\"href\", \"profile.php?user=" + id + "\");\'>" + name + "</strong></a>" +
            (verified?" <span title='Verified' class='badge badge-success'>✔</span>":"") );
        outerDiv.append(row);

        row = newRow();
        row.append(newSpan(4, "<span class='label label-warning'><?=$searchText['GENDER']?>:</span>: " + (gender == 1? 'M':'F')));
        row.append(newSpan(8, "<span class='label label-warning'><?=$searchText['LOCATION']?>:</span>: " + location));
        outerDiv.append(row);

        row = newRow();
        var priceRange = <?=json_encode(getJSON('../data/priceRange.json'))?>;
        row.append(newSpan(4, "<span class='label label-warning'><?=$searchText['PRICE']?>:</span>: " + priceRange[parseInt(price)-1]));
        row.append(newSpan(8, "<span class='label label-warning'><?=$searchText['SUBJECTS']?>:</span>: " + subjects));
        outerDiv.append(row);

        outerDiv.append("<span>" + description + "</span>");
        return outerDiv;
    }
    <? 
        // include_once('../data/searchTutorResult.php');
    ?>
    function appendTable(dataObj){
        $('#resultTable').empty();
        _.each(dataObj, function(item){
            $('#resultTable').append(createRow(item.id, item.name, item.gender, item.district_names, item.price_range, item.subj_names, item.description, item.verified));
        });
    }

    function updateResult(){
        var data = {};
        if($('#genderTab .active a').data('gender')){
            data.gender = $('#genderTab .active a').data('gender');
        }
        if($('#priceTab .active a').data('price-range')){
            data.priceRange = $('#priceTab .active a').data('price-range');
        }
        if($('#districtTab .active a').data('district-id')){
            data.district = [];
            $('#districtTab .active a').each(function(){
                data.district.push($(this).data('district-id'));
            });
        }
        $.ajax({
            url: '../data/searchTutor.php',
            type: 'POST',
            data: data,
            success: function(response) {
                appendTable(response);
                // console.log("updated");
            }
        });
    }

    // called when dom elements are ready
    $(function() {
        updateResult();
        $('#genderTab a, #priceTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $('#districtTab a').click(function (e) {
            e.preventDefault();
            if($(this).data('district-id') == '0'){
                $('#districtTab .active').each(function(){
                    $(this).removeClass('active');
                });
                $(this).parent().addClass('active');
            } else {
                $(this).parent().toggleClass('active');
                if($('#districtTab .active').length){
                    $('#districtTab li:first-child').removeClass('active');
                } else {
                    $('#districtTab li:first-child').addClass('active');
                }
            }
            // $(this).tab('show');
        });
        $('#filter li').click(function(e){
            updateResult();
        });
    });
</script>