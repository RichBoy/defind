<?php
    include 'header.php';
    $target = 'termsConditions';

    $termsConditions = getLangJSON('../data/'.$target.'.json');
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <h4><?=$termsConditions['TITLE']?></h4>
                    </div>
                    <div class="span9">
                        <div class="row">
                            <p class="lead sub-title"><?=$termsConditions['TITLE']?></p>
                            <hr>
                            <p class="content"><strong><?=$termsConditions['SUBCONTENT']?></strong></p>
                        </div>
                        <br />
                        <?
                            // Show all content in the json
                            foreach($termsConditions['CONTENT'] as $item){
                                $subtitle = $item['SUBTITLE'];
                                $subcontent = $item['SUBCONTENT'];
                        ?>
                                <div class="row">
                                    <p class="sub-title"><?=$subtitle?></p>
                                    <p class="content"><?=$subcontent?></p>
                                </div>
                                <br />
                        <?
                            }
                        ?>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
</script>