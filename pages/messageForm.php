<?php
    $msgText = getLangJSON('../data/messageForm.json');
?>
<form id="sendMsgForm">
    <fieldset>
        <legend><?=$msgText['TITLE']?></legend>
        <!-- <input class="username" type="text" placeholder="Name"><br/>
        <input class="phone" type="text" placeholder="Phone"><br/>
        <input class="email" type="text" placeholder="Email"><br/>
        <textarea id="profileDescription" rows="6"></textarea><br/> -->
        <textarea id="pmContent" rows="6" placeholder="<?=isset($_SESSION['login_user'])?'':'You need to log in to send message to tutors'?>" <?=isset($_SESSION['login_user'])?'':'disabled'?>></textarea><br/>
        <?/*
        <div class="control-group">
            <?php
                require_once('../lib/recaptchalib.php');
                $publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
                echo recaptcha_get_html($publickey);
            ?>
        </div>
        */?>
        <center>
            <button id="sendMsgBtn" type="submit" class="btn btn-large btn-primary" disabled='disabled'><?=$msgText['SEND']?></button>
        </center>
    </fieldset>
</form>
<!-- Modal -->
<div id="pmWarning" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?=$msgText['TITLE']?></h3>
    </div>
    <div class="modal-body">
        <label>Content: </label><p id="modalPmContent"></p>
        <div class="alert">
            <strong>Warning!</strong> 
            <?=$msgText['WARNING']?>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true"><?=$msgText['CLOSE']?></button>
        <button id="modalSubmit" class="btn btn-primary"><?=$msgText['SEND']?></button>
    </div>
</div>
    <script>
    $('#pmContent').keyup(function(e){
        e.preventDefault();
        var text = $(this).val();
        if(text.length){
            $('#sendMsgBtn').removeAttr('disabled');
        } else {
            $('#sendMsgBtn').attr('disabled', 'disabled');
        }
    });
    $('#sendMsgForm').submit(function(e){
        e.preventDefault();
        var pmContent = $('#sendMsgForm #pmContent').val();
        if(!pmContent){
            alert("<?=$msgText['EMPTY']?>");
            return;
        }
        $('#pmWarning .modal-body #modalPmContent').text(pmContent);
        $('#pmWarning').modal();
    });

    $('#modalSubmit').click(function(e){
        var message = $('#pmContent').val();
            // captcha_resp = $('#recaptcha_response_field').val(),//Captcha is currently disabled
            // captcha_chal = $('#recaptcha_challenge_field').val();//Captcha is currently disabled
        if(!message){
            alert("<?=$msgText['EMPTY']?>");
            return;
        }
        // if(captcha_resp.length > 0){//Captcha is currently disabled
            $.ajax({
                url: '../data/sendMessage.php',
                type: 'POST',
                data: {
                    to: <?=$messageFormTargetId?>,
                    message: message
                    // recaptcha_response_field: captcha_resp,
                    // recaptcha_challenge_field: captcha_chal
                },
                success: function(response) {
                    if(response.result == 'success'){
                        alert("<?=$msgText['SENT']?>");
                        $(location).attr('href', 'pm.php?sendOnly');
                    } else {
                        alert(response.errorMsg);
                    }
                }
            });
        // }//Captcha is currently disabled
    });
</script>