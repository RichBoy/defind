<?php
    include 'header.php';
    $target = 'contactUs';
    $contactUs = getLangJSON('../data/'.$target.'.json');
?>
<script>
    var RecaptchaOptions = {
        theme : 'clean'
    };
</script>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <h4><?=$contactUs['TITLE']?></h4>
                    </div>
                    <div class="span9">
                        <div class="container">
                            <p class="lead sub-title"><?=$contactUs['MESSAGE']?></p>
                        </div>
                        <div class="span6">
                            <div class="well">
                                <div id='errorMsg' class="errorMsg"></div>
                                <form class="form-horizontal" id="contactForm">
                                    <div class="control-group">
                                        <label class="control-label" for="inputName"><?=$contactUs['NAME']?></label>
                                        <div class="controls">
                                            <input type="text" id="inputName" placeholder="<?=$contactUs['NAME']?>" required>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="inputEmail"><?=$contactUs['EMAIL']?></label>
                                        <div class="controls">
                                            <input type="text" id="inputEmail" placeholder="<?=$contactUs['EMAIL']?>" required>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="inputSubject"><?=$contactUs['SUBJECT']?></label>
                                        <div class="controls">
                                            <input type="text" id="inputSubject" placeholder="<?=$contactUs['SUBJECT']?>" required>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="inputDetail"><?=$contactUs['DETAIL']?></label>
                                        <div class="controls">
                                            <textarea id="inputDetail" rows="8" placeholder="<?=$contactUs['DETAIL']?>" required></textarea>
                                        </div>
                                    </div>
                                    <?/*
                                    <div class="control-group">
                                        <?php
                                            require_once('../lib/recaptchalib.php');
                                            $publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
                                            echo recaptcha_get_html($publickey);
                                        ?>
                                    </div>
                                    */?>
                                    <div class="control-group">
                                        <div class="controls">
                                            <button type="submit" class="btn"><?=$contactUs['SEND']?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
    $(function() {
        $('#contactForm').submit(function(e){
            e.preventDefault();
            /*$('#errorMsg').html('');
            var errMsg = '',
                name = $('#inputName').val(),
                email = $('#inputEmail').val(),
                subject = $('#inputSubject').val(),
                detail = $('#inputDetail').val();
            // var captcha_resp = $('#recaptcha_response_field').val(),//Captcha is currently disabled
            //     captcha_chal = $('#recaptcha_challenge_field').val();

            if(name.length == 0) errMsg += 'Please input name.<br />';
            if(email.length == 0) errMsg += 'Please input email.<br />';
            if(detail.length == 0) errMsg += 'Please input detail.<br />';
            // if(captcha_resp.length == 0) errMsg += 'Please input captcha.<br />';//Captcha is currently disabled

            if(subject.length == 0) subject = 'No Subject';

            if(errMsg.length == 0){*/
                $.ajax({
                    url: '../data/verify.php',
                    type: 'POST',
                    data: {
                        name: name,
                        email: email,
                        detail: detail,
                        subject: subject
                        // recaptcha_response_field: captcha_resp,
                        // recaptcha_challenge_field: captcha_chal//Captcha is currently disabled
                    },
                    success: function(response) {
                        if(response.result == 'success'){
                            $('#contactForm button[type="submit"]').attr('disabled','disabled');
                            alert("<?=$contactUs['THANKS']?>");
                        } else {
                            alert(response.errorMsg);
                        }
                    }
                });
            /*} else {
                $('#errorMsg').html(errMsg);
            }*/
        })
    })
</script>