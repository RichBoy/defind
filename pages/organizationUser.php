<?php
    include 'header.php';
    $profileText = getLangJSON('../data/profile.json');
    if(!isset($login_session)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }

    if($_SESSION['usertype'] != 'org' and $_SESSION['usertype'] != 'admin' ){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }

    if($_SESSION['usertype'] == 'admin'){
        if(isset($_GET['uid'])){
            $userId = $_GET['uid'];
        }
    } else {
        $userId = $_SESSION['user_id'];
    }

    if(!isset($userId)){
?>
        <div class="well">
            Error occurred
        </div>
<?
        return;
    }

    $profile = getOrganizationById($userId);
    
    if(!isset($profile['english_name'])) $profile['english_name'] = "";
    if(!isset($profile['chinese_name'])) $profile['chinese_name'] = "";
    if(!isset($profile['email'])) $profile['email'] = "";
    if(!isset($profile['telephone'])) $profile['telephone'] = "";
    if(!isset($profile['address'])) $profile['address'] = "";
    if(!isset($profile['website'])) $profile['website'] = "";
    if(!isset($profile['description'])) $profile['description'] = "";
?>

                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h2 class="media-heading"><?=$profileText['TITLE']?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="span8 offset2">
                        <h2><?=$profileText['ORG']['TITLE']?></h2>
                        <br />
                        <form id="organizationForm" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="inputEngName"><?=$profileText['ORG']['ENGLISH_NAME']?></label>
                                <div class="controls">
                                    <input type="text" id="inputEngName" placeholder="<?=$profileText['ORG']['ENGLISH_NAME']?>" value="<?=$profile['english_name']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputChiName"><?=$profileText['ORG']['CHINESE_NAME']?></label>
                                <div class="controls">
                                    <input type="text" id="inputChiName" placeholder="<?=$profileText['ORG']['CHINESE_NAME']?>" value="<?=$profile['chinese_name']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputEmail"><?=$profileText['ORG']['EMAIL']?></label>
                                <div class="controls">
                                    <input type="text" id="inputEmail" required placeholder="<?=$profileText['ORG']['EMAIL']?>" value="<?=$profile['email']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputTelephone"><?=$profileText['ORG']['TELEPHONE']?></label>
                                <div class="controls">
                                    <input type="text" id="inputTelephone" placeholder="<?=$profileText['ORG']['TELEPHONE']?>" value="<?=$profile['telephone']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputAddress"><?=$profileText['ORG']['ADDRESS']?></label>
                                <div class="controls">
                                    <input type="text" id="inputAddress" placeholder="<?=$profileText['ORG']['ADDRESS']?>" value="<?=$profile['address']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputWebsite"><?=$profileText['ORG']['WEBSITE']?></label>
                                <div class="controls">
                                    <input type="text" id="inputWebsite" placeholder="<?=$profileText['ORG']['WEBSITE']?>" value="<?=$profile['website']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputDescription"><?=$profileText['ORG']['DESCRIPTION']?></label>
                                <div class="controls">
                                    <textarea id="inputDescription" placeholder="<?=$profileText['ORG']['DESCRIPTION']?>" rows="6"><?=$profile['description']?></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <input type="hidden" id="inputUserId" value="<?=$userId?>" />
                                    <input type="hidden" id="inputAction" value="updateOrg" />
                                    <button type="submit" class="btn"><?=$profileText['UPDATE']?></button>
                                    <span id="organizaionRes" class="inline"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        //do something when selection change
        //data/organization.php?id= XX
        //$('#organizationId').change();
        $('#organizationForm').submit(function(e){
            e.preventDefault();

            var submitBtn = $('#organizationForm button:submit');
            submitBtn.attr('disabled', 'disabled');

            var inputArray = {}
                result = $('#organizaionRes');

            $('#organizationForm input').each(function(){
                var key = $(this).attr('id');
                inputArray[key] = $(this).val();
            });
            inputArray['description'] = $('#inputDescription').val();
            result.addClass('icon-loading');

            $.ajax({
                url: '../data/updateOrg.php',
                type: 'POST',
                data: inputArray,
                success: function(response) {
                    result.removeClass('icon-loading');
                    if(response == "success"){
                        result.addClass('text-success');
                        result.text("<?=$profileText['REMINDER']['SUCCESS']?>");
                    } else {
                        result.addClass('text-error');
                        result.text("<?=$profileText['REMINDER']['FAIL']?>")
                    }
                }
            });
        });
    });
</script>