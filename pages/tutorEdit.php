<?php
include 'header.php';
    if(!isset($login_session) or !isset($isAdmin)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
    $userId = "";
    $action = "";
    if(isset($_GET['user'])) $userId = $_GET['user'];
    if(isset($_GET['action'])) $action = $_GET['action'];

    if($action == 'edit'){
        $profile = json_decode(getProfileEdit($_GET['user']), true);
    }
?>
                <div class="container">
                    <div class="span8 offset2">
                        <?
                            if(!isset($profile['chinese_name'])) $profile['chinese_name'] = "";
                            if(!isset($profile['username'])) $profile['username'] = "";
                            if(!isset($profile['email'])) $profile['email'] = "";
                            if(!isset($profile['lastname'])) $profile['lastname'] = "";
                            if(!isset($profile['firstname'])) $profile['firstname'] = "";
                            if(!isset($profile['availability'])) $profile['availability'] = "1";
                            if(!isset($profile['birth_year'])) $profile['birth_year'] = "";
                            if(!isset($profile['university_id'])) $profile['university_id'] = "0";
                            if(!isset($profile['gender'])) $profile['gender'] = "1";
                            if(!isset($profile['region_id'])) $profile['region_id'] = "0";
                            if(!isset($profile['status'])) $profile['status'] = "1";
                            if(!isset($profile['verified'])) $profile['verified'] = "0";
                        ?>

                        <form id="updateTutorForm" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="inputUserName">User Name</label>
                                <div class="controls">
                                    <input type="text" id="inputUserName" required placeholder="User Name" value="<?=$profile['username']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputEmail">Email</label>
                                <div class="controls">
                                    <input type="text" id="inputEmail" required placeholder="Email" value="<?=$profile['email']?>"/>
                                </div>
                            </div>
                            <? if($action == 'create'){ ?>
                            <div class="control-group">
                                <label class="control-label" for="inputPw">Password</label>
                                <div class="controls">
                                    <input class="password" id="inputPw" type="password" required placeholder="Password">
                                </div>
                            </div>
                            <? } ?>
                            <? if($action == 'edit'){ ?>
                            <div class="control-group">
                                <label class="control-label" for="inputLastName">Last Name</label>
                                <div class="controls">
                                    <input type="text" id="inputLastName" pattern="[a-zA-Z ]{1,20}" required placeholder="Last Name" value="<?=$profile['lastname']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputFirstName">First Name</label>
                                <div class="controls">
                                    <input type="text" id="inputFirstName" pattern="[a-zA-Z ]{1,20}" required placeholder="First Name" value="<?=$profile['firstname']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputChiName">Chinese Name</label>
                                <div class="controls">
                                    <input type="text" id="inputChiName" placeholder="Chinese Name" value="<?=$profile['chinese_name']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputSex">Sex</label>
                                <div class="controls">
                                    <select id="inputSex">
                                        <option value="1"<?=($profile['gender']=="1"?" selected":"")?>>M</option>
                                        <option value="2"<?=($profile['gender']=="2"?" selected":"")?>>F</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputUniversity">University</label>
                                <div class="controls">
                                    <select id="inputUniversity">
                                    <?
                                        $universities = getLangJSON("../data/universityList.json");
                                        foreach($universities as $index => $university){
                                            if($index == 0) continue;
                                            $selected = false;
                                            if($index == $profile['university_id']) $selected = true;
                                            else if($index == 8 && $profile['university_id'] == 0) $selected = true;
                                    ?>
                                        <option value="<?=$index?>"<?=($selected?" selected":"")?>><?=$university?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputBirth">Birth</label>
                                <div class="controls">
                                    <input type="number" min="1950" max="2000" id="inputBirth" placeholder="Birth" value="<?=$profile['birth_year']?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputRegion">Region</label>
                                <div class="controls">
                                    <select id="inputRegion">
                                    <?
                                        $regions = getLangJSON("../data/regionList.json");
                                        foreach($regions as $index => $region){
                                    ?>
                                        <option value="<?=$index?>"<?=($index == $profile['region_id']?" selected":"")?>><?=$region?></option>
                                    <?
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <? } ?>
                            <div class="control-group">
                                <label class="control-label">Activation</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="inputStatus" id="inputStatus1" value="1" <?=($profile['status']=="1"?" checked":"")?>>
                                        Not Active
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="inputStatus" id="inputStatus2" value="2" <?=($profile['status']=="2"?" checked":"")?>>
                                        Active
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Verify</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="inputVerify" id="inputVerify1" value="0" <?=($profile['verified']=="0"?" checked":"")?>>
                                        Non verified
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="inputVerify" id="inputVerify2" value="1" <?=($profile['verified']=="1"?" checked":"")?>>
                                        Verified
                                    </label>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <input type="hidden" id="inputUserId" value="<?=$userId?>" />
                                    <input type="hidden" id="inputAction" value="<?=$action?>" />
                                    <button type="submit" class="btn">Update</button>
                                    <span id="updateTutorRes" class="inline"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    

<?php
    include 'footer.php';
?>

<script>
$(function() {
    $('#updateTutorForm').submit(function(e){
        e.preventDefault();
        var inputArray = {},
            result = $('#updateTutorRes');
        $('input, select').each(
            function(){
                var key = $(this).attr('id');
                if($(this).is(':radio')){
                    if($(this).is(':checked')){
                        key = $(this).attr('name');
                    } else return;
                }
                inputArray[key] = $(this).val();
            }
        );
        result.addClass('icon-loading');
        $.ajax({
            url: '../data/updateTutor.php',
            type: 'POST',
            data: inputArray,
            success: function(response) {
                result.removeClass('icon-loading');
                if(response == "success"){
                    result.addClass('text-success');
                    result.text('Update success');
                } else {
                    result.addClass('text-error');
                    result.text('Update failed')
                }
            }
        });
    });
})
</script>