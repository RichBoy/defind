<?php
    include 'header.php';
    $target = 'blog';

    $termsConditions = getLangJSON('../data/'.$target.'.json');
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <h4><?=$termsConditions['TITLE']?></h4>
                    </div>
                    <div class="span9">
                        <div class="row">
                            <p class="lead sub-title"><?=$termsConditions['SUBTITLE']?></p>
                            <hr>
                            <p class="content"><?=$termsConditions['SUBCONTENT']?></p>
                        </div>
                        <br />
                        <div class="row">
                            <p class="content"><?=$termsConditions['MOREINFO']?></p>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
</script>