<?php
// include 'header.php';
    include_once('../private/globalFunction.php');
    include_once('../private/getSession.php');
    if(!isset($login_session) or !isset($isAdmin)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }

$tutorList = getAllTutor();
?>
                <div class="row-fluid">
                    <a href="tutorEdit.php?action=create" class="btn">Create Tutor</a>
                </div>
                <br />
                <div class="row-fluid">
                    <div class="span3"><strong>Name</strong></div>
                    <div class="span4"><strong>Email</strong></div>
                    <div class="span2"><strong>Status</strong></div>
                </div>
                <? foreach($tutorList as $typeData){ ?>
                    <div class="row-fluid">
                        <div class="span3"><?=$typeData['name']?></div>
                        <div class="span4"><?=$typeData['email']?></div>
                        <? $active = 'Not active';
                            if($typeData['status'] == 2){
                                $active  = 'Active';
                            } else if($typeData['status'] == 3){
                                $active  = 'Deleted';
                            }
                        ?>
                        <div class="span2"><?=$active?></div>
                        <? $click = "tutorEdit.php?action=edit&user=".$typeData['id'] ?>
                        <div class="span1">
                            <a href="<?=$click?>" class="icon-pencil"></a>
                            <a href="#confirmModal" role="button" class="icon-trash" data-toggle="modal"></a>
                            <input type="hidden" class="delEmail" value="<?=$typeData['email']?>" />
                            <input type="hidden" class="delId" value="<?=$typeData['id']?>" />
                        </div>
                    </div>
                <? } ?>
                <!-- Modal -->
                <div id="confirmModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="confirmLabel">Confirmation</h3>
                    </div>
                    <div class="modal-body">
                        <p id="delConfContact">Are you sure you want to delete this user?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button id="delConfBtn" class="btn btn-primary">Yes</button>
                        <span id="updateTutorRes" class="inline"></span>
                    </div>
                </div>

<?php
    // include 'footer.php';
?>
<script>
$(function() {
    var delEmail, delId, status;
    $('.icon-trash').click(function(e){
        delEmail = $(this).next('input.delEmail').val();
        delId = $(this).nextAll('input.delId').val();
        status = $(this).parent().prev();
    });

    $('#delConfBtn').click(function(e){
        e.preventDefault();
        var data = {
            inputUserId : delId,
            inputAction : 'delete'
        },
        result = $('#delConfContact');
        result.addClass('muted');
        result.text('loading...');

        $.ajax({
            url: '../data/updateTutor.php',
            type: 'POST',
            data: data,
            success: function(response) {
                result.removeClass('muted');
                if(response == "success"){
                    $('#delConfBtn').attr('disabled','disabled');
                    status.text('Deleted');
                    result.addClass('text-success');
                    result.text('Update success');
                } else {
                    result.addClass('text-error');
                    result.text('Update failed')
                }
            }
        });
    });

    $('#confirmModal').on('show', function () {
        $('#delConfContact').removeClass();
        if(delEmail)
            $('#delConfContact').text('Are you sure you want to delete '+delEmail+'?');
    });
});
</script>