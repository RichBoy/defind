<?php
    include 'header.php';
    $forgotPwText = getLangJSON('../data/forgotPw.json');
?>
<div class="container">
    <div class="span8 offset2">
        <form id="forgotPwForm" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="inputEmail" style="width:250px;margin-right: 10px;"><?=$forgotPwText['ENTER_EMAIL']?></label>
                <div class="controls">
                    <input type="text" id="inputEmail" required placeholder="<?=$forgotPwText['EMAIL']?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <button type="submit" class="btn"><?=$forgotPwText['SEND']?></button>
                    <span id="forgotPwRes" class="inline"></span>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    include 'footer.php';
?>

<script>
$(function() {
    $('#forgotPwForm').submit(function(e){
        e.preventDefault();
        var email = $('#inputEmail').val(),
            result = $('#forgotPwRes');
        result.addClass('icon-loading');

        if(!/^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|asia|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel)\b)$/i.test(email)){
                result.removeClass('icon-loading');
                result.addClass('text-error');
                result.text("<?=$forgotPwText['INVALID_EMAIL']?>");
                return;
        }

        $.ajax({
            url: '../private/forgotPassword.php',
            type: 'POST',
            data: { email : email},
            success: function(response) {
                result.removeClass('icon-loading');
                if(response.result && response.result == "success"){
                    result.addClass('text-success');
                    result.text("<?=$forgotPwText['SUCCESS']?>");
                } else {
                    result.addClass('text-error');
                    result.text("<?=$forgotPwText['FAIL']?>");
                }
            }
        });
    })
});
</script>