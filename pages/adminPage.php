<?php
include 'header.php';
    if(!isset($login_session) or !isset($isAdmin)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
?>

                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <ul id="adminTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#tutorList" data-toggle="tab">Tutor List</a>
                    </li>
                    <li><a href="organizationlist.php">Organization</a></li>
                </ul>
                <!-- <div id="content" class="container">
                </div> -->
                <div class="tab-content">
                    <div class="tab-pane active" id="tutorList"></div>
                    <div class="tab-pane" id="organization"></div>
                </div>

<?php
    include 'footer.php';
?>
<script>
function tutorlist(){
    $.ajax({
        url: '../pages/tutorlist.php',
        type: 'POST',
        success: function(response) {
            $('#tutorList').html(response);
        }
    });
}
function organization(){
    $.ajax({
        url: '../pages/pm.php',
        type: 'POST',
        success: function(response) {
            $('#organization').html("TODO: change the url above the codes");
            // $('#organization').html(response);
        }
    });
}
$(function() {
    tutorlist();
    // organization();
});
</script>