<?php
    include 'header.php';
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                </div>
                <div class="row">
                    <div class="span3">
                        <h4>{{CAREER}}</h4>
                    </div>
                    <div class="span9">
                        <div class="row">
                            <p class="sub-title">{{成為 Defind 的一份子}}</p>
                            <hr>
                            <p class="content">
                                無論你只想尋找新挑戰還是希望得到實習機會，歡迎你加入我們！請聯絡我們得到更多有關資訊。
                            </p>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
</script>