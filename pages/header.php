<?php
    include_once('../private/globalFunction.php');
    $headerBtn = getLangJSON('../data/headerBtn.json');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?=$headerBtn['TITLE']?></title>
        <meta name="description=" content="Defind takes pride in providing a platform where students and tutors are able to look for one another. We offer high quality services that satisfy the needs of both students and tutors based on our key objectives.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSS -->
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet">

        <!-- JS LIB -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script src="../lib/underscore.min.js"></script>
        <script>
        /* TODO
        //not showing in DOM but still included
        <?
            include '../private/getSession.php';
        ?>
        */
        //google analytics
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-40855355-1', 'defind.com.hk');
            ga('send', 'pageview');
        </script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="."><?=$headerBtn['TUTORS']?></a>
                    <div id="header" class="nav-collapse collapse">
                        <ul class="nav">
                    <?
                        $header = getLangJSON('../data/headerBar.json');
                        // Append all elements declared in the headerBar.json to the header bar
                        foreach($header as $key => $url){
                    ?>
                            <li><a href="<?=$url?>"><?=$key?></a></li>
                    <?
                        }
                    ?>
                        </ul>
                    <?
                        $lang = "Eng";
                        if($LANGUAGE == "english"){
                            $lang = "中文";
                        } 
                    ?>
                    <?
                        if(isset($login_session)){
                    ?>
                        <span style="color:white;" class="pull-right">
                            <?=$headerBtn['WELCOME']?>: <?=$login_session?>
                        <?  if(isset($isAdmin)){ ?>
                            <a href="adminPage.php" class="btn"><i class="icon-user"></i><?=$headerBtn['ADMIN']?></a>
                        <?  } else {?>
                            <a href="pm.php" class="btn"><i class="icon-envelope"></i><?=$headerBtn['PM']?></a>
                            <a href="<?=$_SESSION['usertype'] == 'org'? 'organizationUser.php' : 'myProfile.php'?>" class="btn"><i class="icon-user"></i><?=$_SESSION['usertype'] == 'tutor'?$headerBtn['TUTOR_PROFILE']:$headerBtn['STUDENT_PROFILE']?></a>
                        <?  } ?>
                            <a id="logoutBtn" class="btn"><i class="icon-remove"></i><?=$headerBtn['LOGOUT']?></a>
                            <input id="changeLangBtn" type="button" class="btn" name="<?=$LANGUAGE?>" value="<?=$lang?>">
                        </span>
                    <?
                        } else {
                    ?>
                        <form id="topLoginForm" class="navbar-form pull-right">
                            <!-- <input id="usernameInput" class="span2" type="text" placeholder="Username">
                            <input id="passwordInput" class="span2" type="password" placeholder="Password"> -->
                            <button id="loginBtn" type="submit" class="btn"><?=$headerBtn['LOGIN']?></button>
                            <input id="changeLangBtn" type="button" class="btn" name="<?=$LANGUAGE?>" value="<?=$lang?>">
                        </form>
                    <?
                        }
                    ?>
                        <script>
                            $(function() {
                                $('#logoutBtn').click(function(e){
                                    e.preventDefault();
                                    $.ajax({
                                        url: '../private/logout.php',
                                        success: function(response) {
                                            // console.log(response);
                                            if(response == "success"){
                                                // alert("logged out");
                                                // session destroyed
                                                $(location).attr('href', 'index.php');
                                            } else {
                                                alert("Cannot logout, reason: unknown");
                                                // TODO
                                            }
                                        }
                                    });
                                });

                                $('#loginBtn').click(function(e){
                                    e.preventDefault();
                                    $(location).attr('href', 'login.php');
                                });
                            });
                        </script>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <script>
        function insertParam(key, value)
        {
            key = escape(key); value = escape(value);
            var kvp = document.location.search.substr(1).split('&');
            var i=kvp.length; var x; while(i--) 
            {
                x = kvp[i].split('=');

                if (x[0]==key)
                {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }
            if(i<0) {kvp[kvp.length] = [key,value].join('=');}
            //this will reload the page, it's likely better to store this until finished
            kvp = kvp.join('&');
            document.location.search = kvp;
        }
        $('#changeLangBtn').click(function(e){
            var target = $(this).attr('name') == "chinese"? "english" : "chinese";
            insertParam('language',target);
        });
        </script>
        <div id="wrap">
            <div id="topbarPad" class="visible-desktop"></div>
            <div class="container">