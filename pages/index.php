<?php
    include 'header.php';
    $indexText = getLangJSON('../data/homepage.json');
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading"><?=$indexText['TITLE']?></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="span2">
                        <h5><?=$indexText['UNIVERSITY']?></h5>
                        <ul id="universityTab" class="nav nav-pills nav-stacked">
                            <?
                                $university = getLangJSON('../data/universityList.json');
                                // Append all elements declared in the universityList.json
                                foreach($university as $index => $item){
                            ?>
                                    <li><a href="#" value="<?=$index?>"><?=$item?></a></li>
                            <?
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="span2 offset1">
                        <h5><?=$indexText['REGION']?></h5>
                        <ul id="regionTab" class="nav nav-pills nav-stacked">
                            <?
                                $region = getLangJSON('../data/regionList.json');
                                // Append all elements declared in the regionList.json
                                foreach($region as $index => $item){
                            ?>
                                    <li><a href="#" value="<?=$index?>"><?=$item?></a></li>
                            <?
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="span2 offset1">
                        <h5><?=$indexText['STUDY_TYPE']?></h5>
                        <ul id="typeTab" class="nav nav-pills nav-stacked">
                            <?/*
                                $studyType = getJSON('../data/studyTypeList.json');
                                // Append all elements declared in the studyTypeList.json
                                foreach($studyType as $key => $item){
                            ?>
                                    <li><a href="#" value="<?=$key?>"><?=$item?></a></li>
                            <?
                                }*/
                            ?>
                            <?
                                /*$studyType = getJSON('../data/subject.json');
                                // Append all elements declared in the subject.json
                                foreach($studyType as $item){
                            ?>
                                    <li><a href="#" value='<?=json_encode($item->{'subjects'})?>'><?=$item->{'type'}?></a></li>
                            <?
                                }*/
                            ?>
                            <?
                                // function getStudyType(){
                                //     include_once("../private/config.php");
                                //     $results = DB::query("SELECT name FROM study_types");
                                //     $arr = array();
                                //     foreach($results as $row){
                                //         $arr[] = $row['name'];
                                //     }
                                //     return $arr;
                                // }
                                // $studyType = getStudyType();
                                $studyType = getLangJSON('../data/studyType.json');
                                foreach($studyType as $key => $item){
                            ?>
                                    <li><a href="#" value="<?=$key?>"><?=$item?></a></li>
                            <?
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="span2 offset1">
                        <h5><?=$indexText['SUBJECT']?></h5>
                        <ul id="subjectTab" class="nav nav-pills nav-stacked">
                        <?
                            // Subject data depends on studyType, append here when study type clicked
                        ?>
                        </ul>
                    </div>
                </div>
                <div class="container">
                    <center><button id="submit" class="btn btn-large btn-primary" type="button"><?=$indexText['SEARCH']?></button></center>
                </div>
                <!-- <div id="progressbar"><div class="progress-label">Loading...</div></div> -->
<?php
    include 'footer.php';
?>

<script>
    // updateSubjectTab will be triggered when clicking study type, to update the Subject container
    function updateSubjectTab(response){
        $('#subjectTab li').detach();
        for(var i=0; i<response.length; i++){
            $('#subjectTab').append('<li><a href="#" value="' + response[i].id + '">' + response[i].name + '</a></li>');
        }
        $('#subjectTab a').click(function (e) {
            e.preventDefault();
            $(this).parent().toggleClass('active');
            // $(this).tab('show');
        });
        // $('#subjectTab li:first-child a').click();
    };

    // called when dom elements are ready
    $(function() {
        // Add event to studyType item to trigger updateSubjectTab function
        $('#typeTab a').click(function (e) {
            e.preventDefault();
            // updateSubjectTab(JSON.parse(this.getAttribute('value')));
            $.get('../data/subjectList.php?id=' + this.getAttribute('value'), function(data) {
                updateSubjectTab(data);
            });
        });

        // Add active class to homePage in header bar
        $('#header li:first-child a').click();

        $('.nav-stacked a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        // Add active class to default selection of each container
        $('.nav-stacked li:first-child a').click();

        // Add submit key event to search criteria
        $('#submit').click(function (e) {
            e.preventDefault();
            var university = $('#universityTab .active').text();
            var university_id = $('#universityTab .active a').attr('value');
            var region = $('#regionTab .active').text();
            var region_id = $('#regionTab .active a').attr('value');
            var subjects = [];
            var subjects_id = [];
            if(!$('#subjectTab .active').length){
                alert("<?=$indexText['SUBJECTNOTCHOSE']?>");
                return;
            }

            $('#subjectTab .active').each(
                function(){
                    subjects.push($(this).text());
                    subjects_id.push($(this).children().attr('value'));
                });
            var type = $('#typeTab .active').text();
            var study_id = $('#typeTab .active a').attr('value');
            // console.log("university: " + university);
            // console.log("region: " + region);
            // console.log("subjects: " + subjects);
            // console.log("subjects_id: " + subjects_id);
            // console.log("type: " + type);


            $.ajax({
                url: '../data/search.php',
                type: 'POST',
                data: {
                      university: university,
                      university_id: university_id,
                      region: region,
                      region_id: region_id,
                      type: type,
                      study_id: study_id,
                      subjects: subjects,
                      subjects_id: subjects_id
                },
                success: function(response) {
                    $(location).attr('href', 'searchResult.php');
                }
            });
        });

        // var progressbar = $( "#progressbar" ),
        // progressLabel = $( ".progress-label" );

        // progressbar.progressbar({
        //     value: false,
        //     complete: function() {
        //         progressLabel.text( "Complete!" );
        //     }
        // });
        // progressbar.progressbar( "value", 100);
    });
</script>