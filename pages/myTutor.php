<?php
    include 'header.php';
    if(!isset($login_session) || $_SESSION['usertype'] != "student"){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }
    $ratedOnly = 0;
    if(isset($_GET['ratedOnly'])){
        $ratedOnly = 1;
    }
    $pmText = getLangJSON('../data/pm_rate.json');
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="pm.php"><?=$pmText['RECEIVE']?></a>
                        </li>
                        <li class="">
                            <a href="pm.php?sendOnly"><?=$pmText['SENT']?></a>
                        </li>
                        <li class="<?=($ratedOnly?'':'active')?>">
                            <a href="myTutor.php"><?=$pmText['MYTUTOR']?></a>
                        </li>
                        <li class="<?=($ratedOnly?'active':'')?>">
                            <a href="myTutor.php?ratedOnly"><?=$pmText['RATEDTUTOR']?></a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <table id="requestTable" class="table table-hover span10 offset1">
                        <thead>
                            <tr>
                                <th class="span6"><?=$pmText['TUTOR']?></th>
                                <th class="span4"><?=($ratedOnly?$pmText['RATED']:$pmText['RATE'])?></th>
                            </tr>
                        </thead>
                        <tbody>
                    <? 
                        include_once('../data/myTutor.php');
                        if($ratedOnly){
                            $requests = getMyRatedTutor($_SESSION['user_id']);
                        } else {
                            $requests = getMyNonRatedTutor($_SESSION['user_id']);
                        }
                        foreach($requests as $index => $request){
                    ?>
                            <tr>
                        <?  if($ratedOnly){ ?>
                                <td><a href="profile.php?user=<?=$request['tutor_id']?>"><?=$request['username']?></a></td>
                                <td><?=$pmText['RATED']?></td>
                        <?  } else {?>
                                <td><?=$request['username']?></td>
                                <td><button data-key="<?=$request['date']?>" class="btn btn-primary rateBtn"><?=$pmText['RATE']?></button></td>
                        <?  } ?>
                            </tr>
                    <?
                        }
                    ?>
                        </tbody>
                    </table>
                </div>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        $('#requestTable td .rateBtn').click(function(e){
            e.preventDefault();
            $(this).attr('disabled','disabled');
            $(location).attr('href', 'rateTutor.php?key=' + $(this).data('key'));
        });
    });
</script>