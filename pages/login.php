<?php
    include 'header.php';
    $loginText = getLangJSON('../data/login.json');
    $formText = getLangJSON('../data/form.json');
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span4 offset2">
                        <div class="well">
                            <!-- <form id="loginForm" /*action="../private/login.php" method=""> -->
                            <form id="loginForm">
                                <fieldset>
                                    <legend><?=$loginText['LOGIN']['TITLE']?></legend>
                                    <input class="username" type="text" placeholder="<?=$formText['USERNAME']?>"><br/>
                                    <input class="password" type="password" placeholder="<?=$formText['PASSWORD']?>"><br/>
                                    <!-- <span class="logError" style="display:none;">ERRROORROR</span> -->
                                    <div class="alert alert-error" style="display:none;">ERRRORROR</div>
                                    <label class="checkbox">
                                        <input type="checkbox"> <?=$loginText['LOGIN']['KEEP_LOGGED_IN']?>
                                    </label>
                                    <button id="loginBtn" type="submit" class="btn"><?=$formText['GO']?></button><br/>
                                    <a href="forgotPw.php"><?=$loginText['LOGIN']['FORGOT_PASSWORD']?></a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="well">
                            <form id="registerForm">
                                <fieldset>
                                    <legend><?=$loginText['REGISTER']['TITLE']?></legend>
                                    <label class="radio inline">
                                        <input type="radio" name="userType" value="1" checked>
                                        <?=$loginText['REGISTER']['TUTOR']?>
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" name="userType" value="2">
                                        <?=$loginText['REGISTER']['STUDENT']?>
                                    </label>
                                    <!-- <label class="radio inline">
                                        <input type="radio" name="userType" value="5">
                                        <?=$loginText['REGISTER']['ORGANIZATION']?>
                                    </label> --><br/>
                                    <input class="username" type="text" placeholder="<?=$formText['USERNAME']?>"><span id="usernameReminder"></span><br/>
                                    <!-- <div id="regEmailNotice" class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?=$loginText['REGISTER']['MUSTGMAIL']?>
                                    </div> -->
                                    <input class="email" type="email" placeholder="<?=$formText['EMAIL']?>"><span id="emailReminder"></span><br/>
                                    <input class="password" type="password" placeholder="<?=$formText['PASSWORD']?>"><br/>
                                    <input class="password2" type="password" placeholder="<?=$formText['VERFY_PASSWORD']?>"><i class=""></i><br/>
                                    <?php
                                        require_once('../lib/recaptchalib.php');
                                        $publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
                                        echo recaptcha_get_html($publickey);
                                    ?>
                                    <button type="submit" class="btn"><?=$formText['GO']?></button><i class="icon-loading" style="display:none;margin-left: 10px;"></i>
                                </fieldset>
                                <div id="registerRes" style="display:none;margin-top: 10px;"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="registrationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel"><?=$loginText['REGISTER']['SPAMNOTICEHEADER']?></h3>
                    </div>
                    <div class="modal-body">
                        <p><?=$loginText['REGISTER']['SPAMNOTICECONTENT']?></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>


<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        $('#loginForm').submit(function(e){
            e.preventDefault();
            var username = $('#loginForm .username').val();
            var password = $('#loginForm .password').val();
            if(!username || !password) return;
            $.ajax({
                url: '../private/login.php',
                type: 'POST',
                data: {
                      username: username,
                      password: password
                },
                success: function(response) {
                    // console.log(response);
                    $('#loginForm .alert').hide();
                    if(response == "tutor"){
                        $(location).attr('href', 'myProfile.php');
                    } else if(response == "success"){
                        // alert("logged in");
                        $(location).attr('href', 'index.php');
                    } else {
                        $('#loginForm .alert').text(response);
                        $('#loginForm .alert').fadeIn(250);
                    }
                }
            });
        });

        //registration part start
        $('#registerForm .username').change(function(e){
            e.preventDefault();
            var username = $(this).val();
            var reminder = $('#usernameReminder');
            if(username.length > 5){
                reminder.html("<i class='icon-loading'></i>");
                $.ajax({
                    url: '../data/checkRegister.php',
                    type: 'POST',
                    data: {
                        username: username,
                        action : 'username'
                    },
                    success: function(response) {
                        if(response == 0){
                            reminder.html("<i class='icon-ok'></i>");
                        } else {
                            reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['IDUSED']?></div>");
                        }
                    }
                });
            } else if(username.length){
                reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['IDSHORT']?></div>");
            } else {
                reminder.html("");
            }
        });
        $('#registerForm .password2').change(function(e){
            e.preventDefault();
            var password = $('#registerForm .password').val();
            var password2 = $(this).val();
            if(password.length && password == password2){
                $(this).next().attr("class", "icon-ok");
            } else if(password.length || password2.length){
                $(this).next().attr("class", "icon-remove");
            } else {
                $(this).next().attr("class", "");
            }
        });
        $('#registerForm .email').change(function(e){
            e.preventDefault();
            var email = $(this).val();
            var reminder = $('#emailReminder');
            if(email.length == 0){
                reminder.html("");
            } else if($('#registerForm .email:invalid').length > 0){
                reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['EMAILFORMAT']?></div>");
            } else if(!/^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|asia|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel)\b)$/i.test(email)){
                reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['EMAILFORMAT']?></div>");
            // } else if(email.indexOf("@gmail.com") < 0){
            //     reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['MUSTGMAIL']?></div>");
            } else {
                var statusIcon = $(this).next();
                reminder.html("<i class='icon-loading'></i>");
                $.ajax({
                    url: '../data/checkRegister.php',
                    type: 'POST',
                    data: {
                        email : email,
                        action : 'email'
                    },
                    success: function(response) {
                        if(response == 0){
                            reminder.html("<i class='icon-ok'></i>");
                        } else {
                            reminder.html("<i class='icon-remove'></i><div class='alert alert-error'><?=$loginText['REGISTER']['EMAILUSED']?></div>");
                        }
                    }
                });
            }
        });
        $('#registerForm').submit(function(e){
            e.preventDefault();

            var submitBtn = $('#registerForm button:submit');
            submitBtn.attr('disabled', 'disabled');
            
            var inputArray = {},
                captcha_resp = $('#recaptcha_response_field').val(),
                captcha_chal = $('#recaptcha_challenge_field').val();
                statusIcon = $(this).next();
            statusIcon.show();
            $('#registerForm input').each(function(){
                var key = $(this).attr('class');
                if($(this).is(':radio')){
                    if($(this).is(':checked')){
                        key = $(this).attr('name');
                    } else return;
                }
                inputArray[key] = $(this).val();
            });
            var res = $('#registerRes');
            res.removeClass();
            if(captcha_resp.length > 0){
                inputArray['recaptcha_response_field'] = captcha_resp;
                inputArray['recaptcha_challenge_field'] = captcha_chal;
                $.ajax({
                    url: '../private/register.php',
                    type: 'POST',
                    data: inputArray,
                    success: function(response) {
                        statusIcon.hide();
                        if(response.result && response.result == 'success'){
                            // $('#registrationModal').modal();
                            $('#registerForm')[0].reset();
                            $('#emailReminder').html("");
                            $('#registerForm .password2').next().attr("class", "");
                            $('#usernameReminder').html("");
                            res.addClass('alert alert-success');
                            res.text("<?=$loginText['REGISTER']['REMINDER']['SUCCESS']?>");
                        } else {
                            submitBtn.removeAttr('disabled');
                            res.addClass('alert alert-error');
                            res.text("<?=$loginText['REGISTER']['REMINDER']['FAIL']?>");
                        }
                        res.fadeIn(250);
                    }
                });
            }
        });
    });
</script>