<?php
    include 'header.php';
?>
<?
    function getAllOrganizations(){
        include_once("../private/config.php");
        $results = DB::query("SELECT * FROM organization");
        return $results;
    }
    $organizations = getAllOrganizations();
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <form id="organizationForm">
                    <fieldset>
                        <legend>Organization for admin</legend>
                        <select id="organizationId">
                            <option value="" selected>Create New</option>
                        <? foreach($organizations as $index => $organization){ ?>
                            <option value="<?=$organization['id']?>"><?=$organization['english_name']?></option>
                        <? } ?>
                        </select>
                        <input id="englishName" type="text" placeholder="English name" required><br/>
                        <input id="chineseName" type="text" placeholder="Chinese name" required><br/>
                        <input id="email" type="email" placeholder="E-mail" required><br/>
                        <input id="telphone" type="tel" placeholder="Telephone number" required><br/>
                        <textarea id="description" rows="8" placeholder="Description"></textarea>
                        <input id="url" type="url" placeholder="URL"><br/>
                        <button id="updateBtn" type="submit" class="btn">GO</button><br/>
                    </fieldset>
                </form>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        //do something when selection change
        //data/organization.php?id= XX
        //$('#organizationId').change();
    });
</script>