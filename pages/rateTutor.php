<?php
    include 'header.php';
    if(!isset($_SESSION)){
        session_start();
    }
    if(!isset($login_session)){
?>
        <div class="well">
            User not logged in
        </div>
<?
        return;
    }

    function getTutorDetailForStudentRate($date){
        include_once("../private/config.php");
        // $where = new WhereClause('and');
        // $where->add('tutor_id = %i', $user);
        // $where->add('id in (SELECT student_id FROM student_tutor WHERE tutor_id = %i AND status = 1)', $user);
        // $results = DB::query("SELECT username, student_id, date FROM users "
        //     ."LEFT JOIN student_tutor ON id = student_id WHERE %l", $where);
        $where = new WhereClause('and');
        $where->add('id = (SELECT tutor_id FROM student_tutor WHERE unix_timestamp(date) = %d)', $date);
        $results = DB::query("SELECT id, username FROM users WHERE %l", $where);
        if(DB::count() == 1){
            return $results[0];
        }
        return false; 
    }

    $date = 0;
    if(isset($_GET['key'])){
        $date = $_GET['key'];
    }
    $tutor = getTutorDetailForStudentRate($date);
    $_SESSION['rateTutor'] = $tutor['id'];
    $profileText = getLangJSON('../data/profile.json');
    $pmText = getLangJSON('../data/pm_rate.json');
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="span8 offset2">
                            <h2><?=$pmText['RATE']?></h2>
                            <div class="row">
                                <div id="profilePicContainer" class="span3">
                                    <!-- <img id="profilePic" alt="avatar" class="img-rounded" src="../avatar/test.jpg"/> -->
                                </div>
                                <div class="span5">
                                    <div class="well"><?=getProfileDescription($tutor['id'])?></div>
                                </div>
                                <!-- <p class="lead">IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?IT IS MY INFO you know?</p> -->
                            </div>
                            <hr>
                                <? $profile = getProfile($tutor['id']);?>
                            <div class="row">
                                <div class="span4"><span style="color: #f90;"><?=$profileText['PROFILE']['NAME']?>:</span> <?=$profile['lastname']?> <?=$profile['firstname']?></div>
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['GENDER']?>:</span> <?=($profile['gender'] == ""? "N/A": ($profile['gender'] == 1? 'M':'F'))?></div>
                                <div class="span2"><span style="color: #f90;">GPA:</span> <?=($profile['gpa'] == 0? "N/A": $profile['gpa'])?></div>
                            </div>
                            <div class="row">
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['UNIVERSITY']?>:</span> <?=$profile['university']?></div>
                                <? $facultyList = json_decode(file_get_contents("../data/facultyList.json"), true); ?>
                                <div class="span2"><span style="color: #f90;">Faculty:</span> <?=$facultyList[$profile['faculty_id']]?></div>
                                <div class="span2"><span style="color: #f90;"><?=$profileText['PROFILE']['BIRTH']?>:</span> <?=$profile['birth_year']?></div>
                            </div>
                            <hr>
                            <form class="form-horizontal span6" id="rateForm">
                                <div class="control-group">
                                    <label class="control-label" for="inputAttitude"><?=$pmText['ATTITUDE']?></label>
                                    <div class="controls">
                                        <div id="inputAttitude" class="slider slider-horizontal" data-slider-max=100 data-slider-value=50></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputClarity"><?=$pmText['CLARITY']?></label>
                                    <div class="controls">
                                        <div id="inputClarity" class="slider slider-horizontal" data-slider-max=100 data-slider-value=50></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputKnowledge"><?=$pmText['KNOWLEDGE']?></label>
                                    <div class="controls">
                                        <div id="inputKnowledge" class="slider slider-horizontal" data-slider-max=100 data-slider-value=50></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputPreparedness"><?=$pmText['PREPAREDNESS']?></label>
                                    <div class="controls">
                                        <div id="inputPreparedness" class="slider slider-horizontal" data-slider-max=100 data-slider-value=50></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputComment"><?=$pmText['COMMENT']?></label>
                                    <div class="controls">
                                        <textarea id="inputComment" rows="8" placeholder="<?=$pmText['COMMENT']?>"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                        <button type="submit" class="btn"><?=$pmText['SEND']?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
<?php
    include 'footer.php';
?>
<script src="../bootstrap/js/bootstrap-slider.js"></script>
<script>
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '../bootstrap/css/slider.css') );
    // called when dom elements are ready
    $(function() {
        $('.slider').slider().on('slideStop', function(e){
            $(this).data('slider-value', e.value);
        });
        $('#rateForm').submit(function(e){
            e.preventDefault();
            var data = {
                attitude: $('#inputAttitude').data('slider-value'),
                clarity: $('#inputClarity').data('slider-value'),
                knowledge: $('#inputKnowledge').data('slider-value'),
                preparedness: $('#inputPreparedness').data('slider-value'),
                comment: $('#inputComment').val()
            };
            $('#rateForm button[type="submit"]').attr('disabled','disabled');
            $.ajax({
                url: '../data/rateTutor.php',
                type: 'POST',
                data: data,
                success: function(response){
                    if(response == 'notTutor'){
                        alert("<?=$tutor['username']?> is not confirmed being your tutor");
                    } else if(response == 'failure'){
                        alert("Rate failed");
                    } else if(response == 'success'){
                        alert('Rated. Thank you');
                        $(location).attr('href', 'myTutor.php');
                    }
                }
            });
        });
    });
</script>