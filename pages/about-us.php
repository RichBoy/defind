<?php
    include 'header.php';
    $target = isset($_GET['url'])? $_GET['url'] : null;
    // by default it is aboutUs, it will changes to careers if url = careers
    if(!isset($target)){
        $target = 'aboutUs';
    }
    $aboutUs = getLangJSON('../data/'.$target.'.json');
?>
                <div class="row visible-desktop">
                    <!--LOGO-->
                    <div class="span3">
                        <img src="../images/logo_subpage.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <h4><?=$aboutUs['TITLE']?></h4>
                    </div>
                    <div class="span9">
                        <?
                            // Show all content in the json
                            foreach($aboutUs['CONTENT'] as $item){
                                $subtitle = $item['SUBTITLE'];
                                $subcontent = $item['SUBCONTENT'];
                        ?>
                                <div class="row">
                                    <p class="sub-title lead"><?=$subtitle?></p>
                                    <p class="content"><?=$subcontent?></p>
                                </div>
                        <?
                            }
                        ?>
                    </div>
                </div>
<?php
    include 'footer.php';
?>

<script>
</script>