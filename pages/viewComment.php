<?php
    include 'header.php';
    $profileText = getLangJSON('../data/profile.json');
    $pmText = getLangJSON('../data/pm_rate.json');
    $tutor_id = $_GET['user'];
?>
                <!-- Visible to desktop only -->
                <div class="row visible-desktop">
                    <div class="span12">
                        <div id="logoDiv" class="media">
                            <a class="pull-left" href="#">
                                <img class="media-object" data-src="holder.js/64x64" src="../images/logo.png">
                            </a>
                            <div class="media-body">
                                <h2 class="media-heading"><?=$profileText['TITLE']?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="span8 offset2">
                    <?
                        $rateList = getAvgRate($tutor_id);
                        if(count($rateList) > 0){
                            $overall = round(($rateList['avgAttitude'] + $rateList['avgClarity'] + $rateList['avgKnowledge'] + $rateList['avgPreparedness']) / 4);
                    ?>
                            <table id="averageRate" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><?=$pmText['TOTALRATE']?></th>
                                        <th><?=$pmText['ATTITUDE']?></th>
                                        <th><?=$pmText['CLARITY']?></th>
                                        <th><?=$pmText['KNOWLEDGE']?></th>
                                        <th><?=$pmText['PREPAREDNESS']?></th>
                                        <th><?=$pmText['OVERALL']?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=$rateList['total']?></td>
                                        <td><span class="label"><?=$rateList['avgAttitude']?></span></td>
                                        <td><span class="label"><?=$rateList['avgClarity']?></span></td>
                                        <td><span class="label"><?=$rateList['avgKnowledge']?></span></td>
                                        <td><span class="label"><?=$rateList['avgPreparedness']?></span></td>
                                        <td><span class="label"><?=$overall?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                    <?
                        }
                    ?>
                        </div>
                    </div>
                    <hr>
            <?  $getRate = getRate($tutor_id);
                foreach($getRate as $index => $record){
            ?>
                    <div class="row">
                        <div class="span8 offset2"><strong><?=$record['studentName']?></strong></div>
                    </div>
                    <div class="row">
                        <div class="span3 offset2">
                            <?=$pmText['ATTITUDE']?>: <?=$record['attitude']?><br>
                            <?=$pmText['CLARITY']?>: <?=$record['clarity']?><br>
                            <?=$pmText['KNOWLEDGE']?>: <?=$record['knowledge']?><br>
                            <?=$pmText['PREPAREDNESS']?>: <?=$record['preparedness']?>
                        </div>
                        <div class="span5">
                            <?=$record['comment']?>
                        </div>
                    </div>
                    <hr>
            <?  } ?>
                </div>
<?php
    include 'footer.php';
?>

<script>
    // called when dom elements are ready
    $(function() {
        $('#averageRate .label').each(function(){
            var point = $(this).text();
            if(point < 50) $(this).addClass('label-important');
            else if(point < 80) $(this).addClass('label-warning');
            else $(this).addClass('label-success');
        });
    });
</script>