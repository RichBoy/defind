<?php
    include_once("../private/config.php");
    include_once("../private/passwordFunction.php");
    
    if(isset($_POST['inputAction'])){
        $action = $_POST['inputAction'];

        if($action == 'updateOrg'){
            if(isset($_POST['inputUserId'])){
                $user_id = $_POST['inputUserId'];
                updateProfile($_POST, $user_id);
                echo "success";
            }
        } else if($action == 'delete'){
            if(isset($_POST['inputUserId'])){
                delOrg($_POST['inputUserId']);
                echo "success";
            }
        }
    }

function updateProfile($data, $uid){
    $updateProfArr = array(
        'user_id' => $uid,
        'english_name' => $data['inputEngName'],
        'chinese_name' => $data['inputChiName'],
        'email' => $data['inputEmail'],
        'telephone' => $data['inputTelephone'],
        'address' => $data['inputAddress'],
        'website' => $data['inputWebsite'],
        'description' => $data['description']
    );
    DB::insertUpdate('organization', $updateProfArr);

    $updateUserArr = array(
        'email' => $data['inputEmail']
    );
    DB::update('users', $updateUserArr , "id=%d", $uid);
}

function delOrg($uid){
    $updateUserArr = array(
        'status' => 3
    );
    DB::update('users', $updateUserArr , "id=%d", $uid);
}

?>