<?php
    include_once("../private/config.php");
    if(!isset($_SESSION)){
        session_start();
    }
    $subjects = $_POST['subjects'];
    $subjects = json_decode($subjects);
    $user_id = $_SESSION['user_id'];

    //Remove all before inserting
    DB::delete('tutor_listing_subjects', "user_id=%d", $user_id);

    $insertArr = array();
    foreach($subjects as $subject){
        $insertArr[] = array(
            'subject_id' => $subject,
            'user_id' => $user_id
        );
    }
    DB::insert('tutor_listing_subjects', $insertArr);
    echo "success";
?>