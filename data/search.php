<?php
    $university = $_POST['university'];
    $university_id =  $_POST['university_id'];
    $region = $_POST['region'];
    $region_id =  $_POST['region_id'];
    $type =  $_POST['type'];
    $study_id =  $_POST['study_id'];
    $subjects = $_POST['subjects'];
    $subjects_id = $_POST['subjects_id'];
    if(!isset($university) || !isset($university_id)|| !isset($region) || !isset($region_id)|| !isset($type) || !isset($study_id) || !is_array($subjects) || !is_array($subjects_id)){
        echo "failure";
        return;
    }
    if(!isset($_SESSION)){
        session_start();
    }
    $_SESSION['searchParam'] = array(
        "university" => $university,
        "university_id" => $university_id,
        "region" => $region,
        "region_id" => $region_id,
        "type" => $type,
        "study_id" => $study_id,
        "subjects" => $subjects,
        "subjects_id" => $subjects_id
    );
    echo "success";
?>