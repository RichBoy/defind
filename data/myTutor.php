<?php
    include_once('../private/config.php');
    // function getRequest($user){
    //     $where = new WhereClause('and');
    //     $where->add('tutor_id = %i', $user);
    //     $where->add('id in (SELECT student_id FROM student_tutor WHERE tutor_id = %i AND status = 1)', $user);
    //     $results = DB::query("SELECT username, student_id, date FROM users "
    //         ."LEFT JOIN student_tutor ON id = student_id WHERE %l", $where);
    //     return $results;
    // }
    function getMyNonRatedTutor($student){
        $where = new WhereClause('and');
        $where->add('id in (SELECT tutor_id FROM student_tutor WHERE student_id=%i AND status=2 AND tutor_id NOT IN (SELECT tutor_id FROM tutor_rating WHERE student_id=%i))', $student, $student);
        $results = DB::query("SELECT username, unix_timestamp(date) date FROM users LEFT JOIN student_tutor ON tutor_id = id WHERE %l AND student_id=%i", $where, $student);
        return $results;
    }
    function getMyRatedTutor($student){
        $where = new WhereClause('and');
        $where->add('id in (SELECT tutor_id FROM student_tutor WHERE student_id=%i AND status=2 AND tutor_id IN (SELECT tutor_id FROM tutor_rating WHERE student_id=%i))', $student, $student);
        $results = DB::query("SELECT username, tutor_id FROM users LEFT JOIN student_tutor ON tutor_id = id WHERE %l AND student_id=%i", $where, $student);
        return $results;
    }
?>