<?php
    include_once('../private/config.php');
    function getRequest($user){
        $where = new WhereClause('and');
        $where->add('tutor_id = %i', $user);
        $where->add('id in (SELECT student_id FROM student_tutor WHERE tutor_id = %i AND status = 1)', $user);
        $results = DB::query("SELECT username, student_id, date FROM users "
            ."LEFT JOIN student_tutor ON id = student_id WHERE %l", $where);
        return $results;
    }
    function getAcceptedRequest($user){
        $where = new WhereClause('and');
        $where->add('tutor_id = %i', $user);
        $where->add('id in (SELECT student_id FROM student_tutor WHERE tutor_id = %i AND status = 2)', $user);
        $results = DB::query("SELECT username, student_id, date FROM users "
            ."LEFT JOIN student_tutor ON id = student_id WHERE %l", $where);
        return $results;
    }
?>