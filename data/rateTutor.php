<?php
    include_once('../private/config.php');

    if(!isset($_SESSION)){
        session_start();
    }

    if(!isset($_POST['attitude']) || !isset($_POST['clarity']) || !isset($_POST['knowledge']) || !isset($_POST['preparedness']) || !isset($_POST['comment']) || !isset($_SESSION['user_id']) || !isset($_SESSION['rateTutor'])){
        echo "failure";
        return;
    }

    $attitude = $_POST['attitude'];
    $clarity = $_POST['clarity'];
    $knowledge = $_POST['knowledge'];
    $preparedness = $_POST['preparedness'];
    $comment = $_POST['comment'];
    $id = $_SESSION['user_id'];
    $tutor = $_SESSION['rateTutor'];

    DB::query("SELECT * FROM student_tutor WHERE student_id=%i AND tutor_id=%i AND status=2", $id, $tutor);
    if(DB::count()==0){
        echo "notTutor";
        return;
    }

    DB::insertUpdate('tutor_rating', array(
      'tutor_id' => $tutor,
      'student_id' => $id,
      'attitude' => $attitude,
      'clarity' => $clarity,
      'knowledge' => $knowledge,
      'preparedness' => $preparedness,
      'comment' => $comment
    ));
    echo "success";
?>