<?php
    include_once('../private/config.php');
    header('Content-Type: application/json');

    function getPmCountBetweenTwoUser($user1, $user2){
        include_once("../private/config.php");
        $condition1 = new WhereClause('and');
        $condition1->add('receiver_id = %i', $user1);
        $condition1->add('sender_id = %i', $user2);

        $condition2 = new WhereClause('and');
        $condition2->add('receiver_id = %i', $user2);
        $condition2->add('sender_id = %i', $user1);

        DB::query(
            "SELECT message, receiver_id, sender_id, sender.username sender_name, receiver.username receiver_name, send_time ".
            "FROM private_messages ".
            "LEFT JOIN (SELECT id, username FROM users) sender ON sender.id = sender_id ".
            "LEFT JOIN (SELECT id, username FROM users) receiver ON receiver.id = receiver_id ".
            "WHERE %l OR %l".
            "ORDER BY send_time DESC ", $condition1, $condition2);
        return DB::count();
    }

    function getPmBetweenTwoUser($user1, $user2, $page = 0, $itemPerPage = 10){
        include_once("../private/config.php");
        $condition1 = new WhereClause('and');
        $condition1->add('receiver_id = %i', $user1);
        $condition1->add('sender_id = %i', $user2);

        $condition2 = new WhereClause('and');
        $condition2->add('receiver_id = %i', $user2);
        $condition2->add('sender_id = %i', $user1);

        $results = DB::query(
            "SELECT message, receiver_id, sender_id, sender.username sender_name, receiver.username receiver_name, send_time ".
            "FROM private_messages ".
            "LEFT JOIN (SELECT id, username FROM users) sender ON sender.id = sender_id ".
            "LEFT JOIN (SELECT id, username FROM users) receiver ON receiver.id = receiver_id ".
            "WHERE %l OR %l".
            "ORDER BY send_time DESC ".
            "LIMIT %i, %i", $condition1, $condition2,
            $page * $itemPerPage, $itemPerPage);
        return $results;
    }

    function getUserPmCount($user, $isSend){
        include_once("../private/config.php");
        $where = new WhereClause('and');
        if($isSend)
            $where->add('sender_id = %i', $user);
        else
            $where->add('receiver_id = %i', $user);

        DB::query(
            "SELECT message, receiver_id, sender_id, sender.username sender_name, receiver.username receiver_name, send_time ".
            "FROM private_messages ".
            "LEFT JOIN (SELECT id, username FROM users) sender ON sender.id = sender_id ".
            "LEFT JOIN (SELECT id, username FROM users) receiver ON receiver.id = receiver_id ".
            "WHERE %l ".
            "ORDER BY send_time DESC ", $where);
        return DB::count();
    }

    function getUserPm($user, $isSend, $page = 0, $itemPerPage = 10){
        include_once("../private/config.php");
        $where = new WhereClause('and');
        if($isSend)
            $where->add('sender_id = %i', $user);
        else
            $where->add('receiver_id = %i', $user);

        $results = DB::query(
            "SELECT message, receiver_id, sender_id, sender.username sender_name, receiver.username receiver_name, send_time ".
            "FROM private_messages ".
            "LEFT JOIN (SELECT id, username FROM users) sender ON sender.id = sender_id ".
            "LEFT JOIN (SELECT id, username FROM users) receiver ON receiver.id = receiver_id ".
            "WHERE %l ".
            "ORDER BY send_time DESC ".
            "LIMIT %i, %i", $where,
            $page * $itemPerPage, $itemPerPage);
        return $results;
    }

    function getPM($page, $target_id, $sendOnly, $getTotalPage = false){
        $itemPerPage = 10;
        include_once("../private/config.php");
        if(!isset($_SESSION)){
            session_start();
        }
        $user = $_SESSION['user_id'];
        if($getTotalPage){
            if($target_id > 0)
                $results = getPmCountBetweenTwoUser($user, $target_id);
            else
                $results = getUserPmCount($user, $sendOnly);
            $results = ceil($results / $itemPerPage);
        } else {
            if($target_id > 0)
                $results = getPmBetweenTwoUser($user, $target_id, $page, $itemPerPage);
            else
                $results = getUserPm($user, $sendOnly, $page, $itemPerPage);
        }
        return $results;
    }

    $page = 0;
    $target_id = -1;
    $sendOnly = 0;
    if(isset($_POST['page'])) $page = $_POST['page'];
    if(isset($_POST['target_id'])) $target_id = $_POST['target_id'];
    if(isset($_POST['sendOnly'])) $sendOnly = $_POST['sendOnly'];

    $pm = getPM($page, $target_id, $sendOnly);
    $count = getPM($page, $target_id, $sendOnly, true);
    $results = array(
        "count" => $count,
        "pm" => $pm
    );

    echo json_encode($results);
?>