<?php
    include_once("../private/config.php");
    if(!isset($_SESSION)){
        session_start();
    }
    // $interests = $_POST['interests'];
    // $interests = json_decode($interests);

    $sports = ($_POST['sports']== 'true'? 1:0);
    $music = ($_POST['music']== 'true'? 1:0);
    $video_games = ($_POST['video_games']== 'true'? 1:0);
    $dramas_movies = ($_POST['dramas_movies']== 'true'? 1:0);
    $art = ($_POST['art']== 'true'? 1:0);
    $books = ($_POST['books']== 'true'? 1:0);
    $language = ($_POST['language']== 'true'? 1:0);
    $travel = ($_POST['travel']== 'true'? 1:0);
    $shopping = ($_POST['shopping']== 'true'? 1:0);
    $user_id = $_SESSION['user_id'];
    //Remove all before inserting
    DB::delete('survey_interest', "id=%i", $user_id);

    $insertArr = array(
        'sports' => $sports,
        'music' => $music,
        'video_games' => $video_games,
        'dramas_movies' => $dramas_movies,
        'art' => $art,
        'books' => $books,
        'language' => $language,
        'travel' => $travel,
        'shopping' => $shopping,
        'id' => $user_id
    );
    DB::insert('survey_interest', $insertArr);
    echo "success";
?>