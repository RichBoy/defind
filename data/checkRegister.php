<?php
    include_once("../private/config.php");
    include_once("../private/globalFunction.php");

    if(isset($_POST['action'])){
        $action = $_POST['action'];

        if($action == 'username'){
            if(isset($_POST['username']))
                echo checkHasUsername($_POST['username']);
        } if($action == 'email'){
            if(isset($_POST['email']))
                echo checkHasEmail($_POST['email']);
        }
    }
?>