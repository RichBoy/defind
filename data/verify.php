<?php
require_once('../lib/recaptchalib.php');
header('Content-Type: application/json');

// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
$privatekey = "6LdDYt8SAAAAAJQ81pyNetMnTVaNLYEYrJws1xaN";

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;

$arr = array();
# was there a reCAPTCHA response?

// Captcha is disabled in current stage
// if ($_POST["recaptcha_response_field"]) {
//     $resp = recaptcha_check_answer ($privatekey,
//                                     $_SERVER["REMOTE_ADDR"],
//                                     $_POST["recaptcha_challenge_field"],
//                                     $_POST["recaptcha_response_field"]);

//     if (!$resp->is_valid) {
//         // What happens when the CAPTCHA was entered incorrectly
//         $arr['result'] = "failed";
//         $arr['errorMsg'] = "wrong captcah: ".$resp->error;
//         echo json_encode($arr);
//         //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again."."(reCAPTCHA said: " . $resp->error . ")");
//     } else {
        // Your code here to handle a successful verification
        $name = $_POST["name"];
        $email = $_POST["email"];
        $detail = $_POST["detail"];
        $subject = $_POST["subject"];

        $to="info@defind.com.hk";
        $message = "
        <html>
        <head>
          <title>$subject</title>
        </head>
        <body>
          <div>From $name</div>
          <div>Email $email</div>
          <div>$detail</div>
        </body>
        </html>
        ";
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "X-Mailer: PHP/".phpversion();

        //if (mail($to,$subject,$body))

        if (mail($to,$subject,$message,$headers)){
            $arr['result'] = "success";
        } else {
            $arr['result'] = "failed";
            $arr['errorMsg'] = "mail not sent";
        }
        echo json_encode($arr);
//     }
// }
?>
