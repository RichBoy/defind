<?php
    include_once("../private/config.php");
    include_once("../private/passwordFunction.php");
    
    if(isset($_POST['inputAction'])){
        $action = $_POST['inputAction'];

        if($action == 'edit'){
            if(isset($_POST['inputUserId'])){
                $user_id = $_POST['inputUserId'];
                updateUser($_POST, $user_id);
                updateProfile($_POST, $user_id);
                echo "success";
            } 
        } else if($action == 'create'){
            insertUser($_POST);
            echo "success";
        } else if($action == 'delete'){
            if(isset($_POST['inputUserId'])){
                delUser($_POST['inputUserId']);
                echo "success";
            }
        } else if($action == 'updatePro'){
            if(isset($_POST['inputUserId'])){
                $user_id = $_POST['inputUserId'];
                updateProfile($_POST, $user_id);
                echo "success";
            }
        } else if($action == 'changePw'){
            if(isset($_POST['inputUserId']) && isset($_POST['inputOldPw']) && isset($_POST['inputNewPw'])){
                $user_id = $_POST['inputUserId'];
                if(checkPw($_POST['inputOldPw'],$user_id)){
                    updatePw($_POST['inputNewPw'], $user_id);
                    echo "success";
                }
            }
        }
    }


function updateUser($data, $uid){
    $updateUserArr = array(
        'email' => $data['inputEmail'],
        'status' => $data['inputStatus'],
        'username' => $data['inputUserName']
    );
    DB::update('users', $updateUserArr , "id=%d", $uid);
}

function updateProfile($data, $uid){
    $updateProfArr = array(
        'user_id' => $uid,
        'birth_year' => $data['inputBirth'],
        'firstname' => $data['inputFirstName'],
        'lastname' => $data['inputLastName'],
        'region_id' => $data['inputRegion'],
        'gender' => $data['inputSex'],
        'university_id' => isset($data['inputUniversity'])? $data['inputUniversity']: 8,
        'faculty_id' => isset($data['inputFaculty'])? $data['inputFaculty']: 0,
        'chinese_name' => $data['inputChiName'],
        'school' => isset($data['inputSchool'])? $data['inputSchool']: 0,
        'gpa' => isset($data['inputGpa'])? $data['inputGpa']: 0
    );
    if(isset($data['inputVerify'])){
        $updateProfArr['verified'] = $data['inputVerify'];
    }
    
    DB::insertUpdate('profiles', $updateProfArr);

    if(!isset($_SESSION)){
        session_start();
    }
    if($_SESSION['usertype'] == 'tutor'){
        $updateTutorArr = array(
            'user_id' => $uid,
            'price_range' => $data['inputPrice'],
            'description' => $data['description']
        );
        DB::insertUpdate('tutor_details', $updateTutorArr);
    }

    if(isset($data['inputDistrict'])){
        $districts = $data['inputDistrict'];
        $districts = json_decode($districts);

        //Remove all before inserting
        DB::delete('profile_districts', "user_id=%d", $uid);

        $insertArr = array();
        foreach($districts as $district){
            $insertArr[] = array(
                'district_id' => $district,
                'user_id' => $uid
            );
        }
        DB::insert('profile_districts', $insertArr);
    }
}

function insertUser($data){
    $date = new DateTime();
    $encPW = encryptPassword($data['inputPw']);
    $updateUserArr = array(
        'email' => $data['inputEmail'],
        'status' => $data['inputStatus'],
        'username' => $data['inputUserName'],
        'password' => $encPW,
        'usertype' => 1,
        'createtime' => $date->getTimestamp()
    );
    DB::insert('users', $updateUserArr);
}

function delUser($uid){
    $updateUserArr = array(
        'status' => 3
    );
    DB::update('users', $updateUserArr , "id=%d", $uid);
}

function checkPw($pw, $uid){
    $encPW = encryptPassword($pw);
    $results = DB::query("SELECT `username` FROM users WHERE id = %i and password = %s", $uid, $encPW);
    if(DB::count() == 1){
        return true;
    };
    return false;
}

function updatePw($pw, $uid){
    $encPW = encryptPassword($pw);
    $updateUserArr = array(
        'password' => $encPW
    );
    DB::update('users', $updateUserArr , "id=%d", $uid);
}
?>