<?php
    include_once('../private/config.php');
    include_once("../private/globalFunction.php");
    header('Content-Type: application/json');

    if(!isset($_SESSION)){
        session_start();
    }
    $searchParam = $_SESSION['searchParam'];

    // if(isset($_REQUEST['minPrice'])){
    //     $minPrice = $_REQUEST['minPrice'];
    // }
    // if(isset($_REQUEST['maxPrice'])){
    //     $maxPrice = $_REQUEST['maxPrice'];
    // }
    // if(isset($_REQUEST['district'])){
    //     $district = $_REQUEST['district'];
    // }
    $where = new WhereClause('and');
    // $where->add('tls.user_id <> %s', '');
    $where->add('subj_ids <> %s', '');

    if(isset($searchParam['university_id']) && $searchParam['university_id'] != 0){
        $where->add('pf.university_id = %d', $searchParam['university_id']);
    }

    if(isset($_REQUEST['priceRange'])){
        $where->add('price_range = %d', $_REQUEST['priceRange']);
    }
    if(isset($_REQUEST['gender'])){
        $where->add('gender=%d', $_REQUEST['gender']);
    }
    if(isset($_REQUEST['district'])){
        $where->add('pf.user_id IN ('
            . '    SELECT user_id FROM profile_districts'
            . '    WHERE district_id IN %li'
            . ')', $_REQUEST['district']);
    } 
    else {
        if($searchParam['region_id'] != 0){
            $where->add('pf.user_id IN ('
                . '    SELECT user_id FROM profile_districts'
                . '    WHERE district_id IN (SELECT id FROM location_districts WHERE id = %i)'
                . ')', $searchParam['region_id']);
        }
    }
    $subjectArr = $searchParam['subjects_id'];
    if(isset($subjectArr)){
        $where->add('pf.user_id IN ('
            . '    SELECT user_id FROM tutor_listing_subjects'
            . '    WHERE subject_id IN %li'
            . ')', $subjectArr);
    }
    
    $results = DB::query(
        "SELECT pf.user_id id, CONCAT(firstname, ' ', lastname) name, chinese_name, gender, birth_year, region_id, university_id, verified, price_range, description, district_ids, district_names, subj_ids, subj_names, detail.availability ".
        "FROM profiles pf\n".
            "LEFT JOIN (SELECT user_id, description, price_range, availability FROM tutor_details ) detail ON pf.user_id = detail.user_id\n"
            . "LEFT JOIN (\n"
            . "SELECT user_id, GROUP_CONCAT(district_id) district_ids, GROUP_CONCAT(name) district_names\n"
            . "FROM profile_districts\n"
            . "    LEFT JOIN location_districts ld ON district_id = ld.id\n"
            . "    GROUP BY user_id\n"
            . ") pd ON pf.user_id = pd.user_id\n"

        ."LEFT JOIN(\n"
            ."SELECT GROUP_CONCAT(subject_id) subj_ids, GROUP_CONCAT(name) subj_names, user_id\n"
            ."FROM (\n"
                ."SELECT * FROM tutor_listing_subjects\n"
                ."LEFT JOIN study_subjects ss ON ss.id = subject_id\n"
                ."WHERE subject_id IN (SELECT id FROM study_subjects WHERE study_id = %d)\n"
            .") temp2 GROUP BY (user_id)\n"
        .") subj ON pf.user_id = subj.user_id\n"

            . "WHERE %l", $searchParam['study_id'], $where->text()
        );
    
    for($i = 0; $i< sizeof($results); $i++){
        $results[$i]['subj_names'] = getSubjectByID($results[$i]['subj_ids']);
        $results[$i]['district_names'] = getDistrictsByID($results[$i]['district_ids']);
    }

    
    // $results = DB::query(
    //     "SELECT pf.user_id id, CONCAT(firstname, ' ', lastname) name, chinese_name, gender, birth_year, region_id, university_id, subj_ids, subj_names, district_ids, district_names, price, description\n"
    //     . "FROM profiles pf\n"
    //     . "    LEFT JOIN (\n"
    //     . "    SELECT GROUP_CONCAT(subject_id) subj_ids, GROUP_CONCAT(name) subj_names, user_id\n"
    //     . "    FROM (\n"
    //     . "        SELECT * FROM tutor_listing_subjects\n"
    //     . "        LEFT JOIN study_subjects ss ON ss.id = subject_id\n"
    //     . "        WHERE subject_id IN (SELECT id FROM study_subjects WHERE study_id = %d)\n"
    //     . "    ) temp\n"
    //     . "GROUP BY (user_id)\n"
    //     . ") tls ON pf.user_id = tls.user_id\n"
    //     . "LEFT JOIN (\n"
    //     . "SELECT user_id, GROUP_CONCAT(district_id) district_ids, GROUP_CONCAT(name) district_names\n"
    //     . "FROM profile_districts\n"
    //     . "    LEFT JOIN location_districts ld ON district_id = ld.id\n"
    //     . "    GROUP BY user_id\n"
    //     . ") pd ON pf.user_id = pd.user_id\n"
    //     . "LEFT JOIN (\n"
    //     . "SELECT user_id, MIN(price) price, GROUP_CONCAT(details) description FROM tutor_listings\n"
    //     . "    WHERE teaching_status = 1\n"
    //     . "    GROUP BY user_id\n"
    //     . ") tl ON tl.user_id = tls.user_id\n"
    //     . "WHERE %l", $searchParam['study_id'], $where->text());
    echo json_encode($results);
?>