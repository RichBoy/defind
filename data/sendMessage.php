<?php
require_once('../lib/recaptchalib.php');
header('Content-Type: application/json');

// Get a key from https://www.google.com/recaptcha/admin/create
$publickey = "6LdDYt8SAAAAAAqNiRd0-cSAmaLK2tDG4Ly5oBAV";
$privatekey = "6LdDYt8SAAAAAJQ81pyNetMnTVaNLYEYrJws1xaN";

# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
$error = null;

function sendPM($receiver, $message){
    include_once("../private/config.php");
    if(!isset($_SESSION)){
        session_start();
    }
    $user = $_SESSION['user_id'];
    DB::insert('private_messages', array(
        'sender_id' => $user,
        'receiver_id' => $receiver,
        'message' => $message
    ));
}

$arr = array();
# was there a reCAPTCHA response?
// if ($_POST["recaptcha_response_field"]) {//Captcha is currently disabled
//     $resp = recaptcha_check_answer ($privatekey,
//                                     $_SERVER["REMOTE_ADDR"],
//                                     $_POST["recaptcha_challenge_field"],
//                                     $_POST["recaptcha_response_field"]);

//     if (!$resp->is_valid) {
//         // What happens when the CAPTCHA was entered incorrectly
//         $arr['result'] = "failed";
//         $arr['errorMsg'] = "wrong captcah: ".$resp->error;
//         echo json_encode($arr);
//         //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again."."(reCAPTCHA said: " . $resp->error . ")");
//     } else {
        // Your code here to handle a successful verification
        $message = $_POST["message"];
        $receiver = $_POST["to"];
        sendPM($receiver, $message);

        $arr['result'] = 'success';
        echo json_encode($arr);
//     }
// }
?>
